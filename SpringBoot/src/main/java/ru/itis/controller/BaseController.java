package ru.itis.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itis.models.Company;
import ru.itis.models.Developer;
import ru.itis.models.Person;
import ru.itis.models.Roles;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/person")
public class BaseController {
    private List<Person> people;

    @PersistenceContext
    private EntityManager entityManager;


    @PostMapping("/add")
    @Transactional
    public ResponseEntity<Developer> addPerson(@RequestBody Developer developer) {
        Company company = entityManager.find(Company.class, 12);

        developer.setCompany(company);
        entityManager.persist(developer);

        return ResponseEntity.ok(developer);
    }

    @GetMapping("/{index}")
    public ResponseEntity<Person> getPersonByIndex(@PathVariable int index) {
        Developer developer = entityManager.find(Developer.class, index);

        return ResponseEntity.ok(developer);
    }

    @GetMapping("/find")
    public ResponseEntity<List<Person>> getPersonByRole(@RequestParam("role") Roles role) {
        log.info("finding person by role = " + role);
        List<Person> list = people.stream().filter(person -> person.getRole().equals(role)).collect(Collectors.toList());

        return ResponseEntity.ok(list);
    }

}
