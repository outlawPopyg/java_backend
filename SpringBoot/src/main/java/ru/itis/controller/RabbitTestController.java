package ru.itis.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequiredArgsConstructor
public class RabbitTestController {
    private final RabbitTemplate template;

    @GetMapping("/emit")
    public ResponseEntity<?> queue1() {
        log.info("Emit to queue1");
        template.convertAndSend("queue1", "Message to queue1");
        return ResponseEntity.ok().build();
    }


    @GetMapping("/parallel")
    public ResponseEntity<?> parallel() {
        log.info("Emit to queue");

        for (int i = 0; i < 10; i++) {
            template.convertAndSend("query-example-2", "Message: " + i);
        }

        return ResponseEntity.ok().build();
    }

    @GetMapping("/exchange")
    public ResponseEntity<?> exchange() {
        log.info("Emit to exchange-example-3");
        template.setExchange("exchange-example-3");

        for (int i = 1; i < 11; i++) {
            template.convertAndSend("message " + i);
        }

        return ResponseEntity.ok().build();
    }

    @GetMapping("/exchange/error")
    public ResponseEntity<?> directExchange() {
        log.info("Emit as error");
        template.setExchange("exchange-example-4");
        template.convertAndSend("error", "Error message");

        return ResponseEntity.ok().build();
    }

    @GetMapping("/exchange/info")
    public ResponseEntity<?> infoExchange() {
        log.info("Emit as info");

        template.convertAndSend("info", "Info message");

        return ResponseEntity.ok().build();
    }

    @GetMapping("/exchange/warning")
    public ResponseEntity<?> warnExchange() {
        log.info("Emit as warning");

        template.convertAndSend("warning", "Warning message");

        return ResponseEntity.ok().build();
    }

    @GetMapping("/exchange/topic/1")
    public ResponseEntity<?> topicExchange1() {
        log.info("Emit topic 1");
        template.setExchange("exchange-example-5");

        template.convertAndSend("orange.com", "I like orange");

        return ResponseEntity.ok().build();
    }

    @GetMapping("/exchange/topic/2")
    public ResponseEntity<?> topicExchange2() {
        log.info("Emit topic 2");
        template.setExchange("exchange-example-5");
        template.convertAndSend("com.rabbit.mq", "I learn rabbitmq");

        return ResponseEntity.ok().build();
    }

    @GetMapping("/exchange/topic/3")
    public ResponseEntity<?> topicExchange3() {
        log.info("Emit topic 3");
        template.setExchange("exchange-example-5");

        template.convertAndSend("org.hibernate.Driver", "Hibernate driver");

        return ResponseEntity.ok().build();
    }
}
