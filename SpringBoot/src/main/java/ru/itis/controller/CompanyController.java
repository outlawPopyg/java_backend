package ru.itis.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import ru.itis.models.Company;
import ru.itis.service.CompanyService;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;


@Slf4j
@RestController
@RequestMapping("/company")
@RequiredArgsConstructor
public class CompanyController {

    private final CompanyService companyService;

    @PersistenceContext
    private EntityManager entityManager;

    @PostMapping("/create")
    @Transactional(isolation = Isolation.DEFAULT)
    public ResponseEntity<Company> createCompany(@RequestBody Company company) {
        log.info("creating company " + company.getName());
        log.info("" + companyService.createCompany(company));

        return ResponseEntity.ok(company);
    }

    @GetMapping("/find/{index}")
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public ResponseEntity<Company> repeatableReadExample(@PathVariable int index) {
        Company company = entityManager.find(Company.class, index);

//        entityManager.lock(company, LockModeType.PESSIMISTIC_READ); // блокируем сущность (имитация того, что делаем запрос на изменеие)



        return ResponseEntity.ok(company);
    }



}
