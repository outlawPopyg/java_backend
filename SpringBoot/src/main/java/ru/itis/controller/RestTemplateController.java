package ru.itis.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import ru.itis.request.UserRequest;
import ru.itis.response.UserResponse;

import java.net.URI;

@RestController
@RequestMapping("/resttemplate")
@Slf4j
public class RestTemplateController {

    public final String RESOURCE_URL = "http://localhost:8081";

    @GetMapping("/getForEntity")
    public ResponseEntity<?> getForEntity() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response =
                restTemplate.getForEntity(RESOURCE_URL + "/users", String.class);

        log.info("Response: " + response);

        return ResponseEntity.ok().build();
    }

    @GetMapping("/getForObject")
    public ResponseEntity<?> getForObject() {
        RestTemplate restTemplate = new RestTemplate();
        UserResponse userResponse = restTemplate
                .getForObject(RESOURCE_URL + "/users/862d661b-2a24-4883-b40b-35727d519365", UserResponse.class);

        System.out.println(userResponse);
        System.out.println(restTemplate.headForHeaders(RESOURCE_URL + "/users/862d661b-2a24-4883-b40b-35727d519365"));

        return ResponseEntity.ok().build();
    }

    @GetMapping("/postForObject")
    public ResponseEntity<?> postForObject() {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<UserRequest> request =
                new HttpEntity<>(UserRequest.builder().username("Ivan").password("ewfsdf").build());

        UserResponse response = restTemplate.postForObject(RESOURCE_URL + "/users", request, UserResponse.class);

        System.out.println(response);

        return ResponseEntity.ok().build();
    }

    @GetMapping("/postForLocation")
    public ResponseEntity<?> postForLocation() {

        RestTemplate restTemplate = new RestTemplate();

        HttpEntity<UserRequest> request =
                new HttpEntity<>(UserRequest.builder().username("Vladimir").password("hui").build());

        URI location = restTemplate.postForLocation(RESOURCE_URL + "/users", request);
        System.out.println(location);

        return ResponseEntity.ok().build();
    }

    @GetMapping("/exchange")
    public ResponseEntity<?> exchange() {
        RestTemplate restTemplate = new RestTemplate();

        HttpEntity<UserRequest> request =
                new HttpEntity<>(UserRequest.builder().username("Iladr").password("iwef").build());

        ResponseEntity<UserResponse> response =
                restTemplate.exchange(RESOURCE_URL + "/users", HttpMethod.POST, request, UserResponse.class);

        UserResponse body = response.getBody();
        System.out.println(body);

        return ResponseEntity.ok().build();
    }

    /* x-www-form-urlencoded */
    @GetMapping("/formData")
    public ResponseEntity<?> formData() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("id", "1");

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = restTemplate.postForEntity(
                RESOURCE_URL + "/form", request, String.class
        );

        return ResponseEntity.ok().build();

    }

}
