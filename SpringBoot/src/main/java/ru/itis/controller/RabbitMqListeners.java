package ru.itis.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.annotation.RabbitListeners;
import org.springframework.stereotype.Component;

@Component
@EnableRabbit
@Slf4j
public class RabbitMqListeners {

    @RabbitListener(queues = "queue1")
    public void processQueue1(String message) {
        log.info(String.format("Received message from: %s", message));
    }


    @RabbitListener(queues = "query-example-2")
    public void worker1(String message) {
        log.info("worker 1 " + message);
    }

    @RabbitListener(queues = "query-example-2")
    public void worker2(String message) throws InterruptedException {
        log.info("worker 2 " + message);
        Thread.sleep(2000);
    }


    @RabbitListener(queues = "queue3")
    public void worker3(String message) {
        log.info("worker 3 " + message);
    }

    @RabbitListener(queues = "queue4")
    public void worker4(String message) throws InterruptedException {
        log.info("worker 4 " + message);
        Thread.sleep(2000);
    }

    @RabbitListener(queues = "queue5")
    public void worker5(String message) throws InterruptedException {
        log.info("worker 5: " + message);
    }

    @RabbitListener(queues = "queue6")
    public void worker6(String message) throws InterruptedException {
        log.info("worker 6: " + message);
    }


    @RabbitListener(queues = "queue7")
    public void worker7(String message) throws InterruptedException {
        log.info("worker 7: " + message);
    }

    @RabbitListener(queues = "queue8")
    public void worker8(String message) throws InterruptedException {
        log.info("worker 8: " + message);
    }


}
