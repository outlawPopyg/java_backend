package ru.itis.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class RabbitConfiguration {

    // настраиваем соединение с RabbitMQ
    @Bean
    public ConnectionFactory connectionFactory() {
        return new CachingConnectionFactory("localhost");
    }

    @Bean
    public AmqpAdmin amqpAdmin(ConnectionFactory connectionFactory) {
        return new RabbitAdmin(connectionFactory);
    }

    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        return new RabbitTemplate(connectionFactory);
    }

    // объявляем очередь с именем queue1
    @Bean
    public Queue myQueue1() {
        return new Queue("queue1");
    }

    @Bean
    public Queue myQueue2() { return new Queue("query-example-2"); }

    @Bean
    public Queue myQueue3() { return new Queue("queue3"); }

    @Bean
    public Queue myQueue4() { return new Queue("queue4"); }

    @Bean
    public FanoutExchange fanoutExchangeA() {
        return new FanoutExchange("exchange-example-3");
    }

    @Bean
    public Binding binding1() {
        return BindingBuilder.bind(myQueue3()).to(fanoutExchangeA());
    }

    @Bean
    public Binding binding2() {
        return BindingBuilder.bind(myQueue4()).to(fanoutExchangeA());
    }

    @Bean
    public Queue myQueue5() { return new Queue("queue5"); }

    @Bean
    public Queue myQueue6() { return new Queue("queue6"); }

    @Bean
    public DirectExchange directExchange() {
        return new DirectExchange("exchange-example-4");
    }

    @Bean
    public Binding errorBinding1() {
        return BindingBuilder.bind(myQueue5()).to(directExchange()).with("error");
    }

    @Bean
    public Binding errorBinding2() {
        return BindingBuilder.bind(myQueue6()).to(directExchange()).with("error");
    }

    @Bean
    public Binding infoBinding() {
        return BindingBuilder.bind(myQueue6()).to(directExchange()).with("info");
    }

    @Bean
    public Binding warningBinding() {
        return BindingBuilder.bind(myQueue6()).to(directExchange()).with("warning");
    }

    @Bean
    public Queue myQueue7() { return new Queue("queue7"); }

    @Bean
    public Queue myQueue8() { return new Queue("queue8"); }

    @Bean
    public TopicExchange topicExchange(){
        return new TopicExchange("exchange-example-5");
    }

    @Bean
    public Binding binding3(){
        return BindingBuilder.bind(myQueue7()).to(topicExchange()).with("orange.*");
    }

    @Bean
    public Binding binding4(){
        return BindingBuilder.bind(myQueue7()).to(topicExchange()).with("*.rabbit.*");
    }

    @Bean
    public Binding binding5(){
        return BindingBuilder.bind(myQueue8()).to(topicExchange()).with("org.#");
    }

}