package ru.itis.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import ru.itis.models.Person;
import ru.itis.models.Roles;

@Configuration
@ComponentScan(basePackages = "ru.itis")
public class SpringConfig {

    @Bean
    @Primary
    public Person getAnotherPerson(Person person) {
        return new Person(person.getName(), 22, Roles.IT);
    }

    @Bean
    public Person getPerson() {
        return new Person("Kalim", 20, Roles.IT);
    }
}
