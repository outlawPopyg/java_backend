package ru.itis.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import ru.itis.models.Company;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Service
public class CompanyService {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional(isolation = Isolation.DEFAULT)
    public Company createCompany(Company company) {
        entityManager.persist(company);
        entityManager.flush();

        return company;
    }
}
