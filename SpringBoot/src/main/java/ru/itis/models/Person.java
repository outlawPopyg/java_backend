package ru.itis.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;

@Data
@MappedSuperclass
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;

    @Column
    String name;

    @Column
    int age;

    @Column
    Roles role;

    public Person() {}

    public Person(String name, int age, Roles role) {
        this.name = name;
        this.role = role;
        this.age = age;
    }
}
