package ru.itis.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@Table
@Data
@EqualsAndHashCode(callSuper = false)
public class Developer extends Person {

    @Column
    String language;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "company_id")
    Company company;

    public Developer() {}

    public Developer(String name, int age, Roles role, String language) {
        super(name, age, role);
        this.language = language;
    }
}
