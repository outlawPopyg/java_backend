package ru.itis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import ru.itis.config.RabbitConfiguration;

@SpringBootApplication
@Import(RabbitConfiguration.class)
public class Program {
    public static void main(String[] args) {
        SpringApplication.run(Program.class, args);
    }

}
