package waitAndNotify.consumer;

import waitAndNotify.broker.MessageBroker;
import waitAndNotify.model.Message;

import java.util.concurrent.TimeUnit;

public class MessageConsumingTask implements Runnable {
    private final MessageBroker messageBroker;

    public MessageConsumingTask(MessageBroker messageBroker) {
        this.messageBroker = messageBroker;
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                TimeUnit.SECONDS.sleep(1);
                Message message = messageBroker.consume();
                System.out.println("[CONSUMED]: " + message);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
