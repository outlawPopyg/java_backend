package waitAndNotify.producer;

import waitAndNotify.broker.MessageBroker;
import waitAndNotify.model.Message;

import java.util.concurrent.TimeUnit;

public class MessageProducingTask implements Runnable{
    private final MessageBroker messageBroker;
    private final MessageFactory messageFactory;

    public MessageProducingTask(MessageBroker messageBroker) {
        this.messageBroker = messageBroker;
        this.messageFactory = new MessageFactory();
    }

    private static class MessageFactory {
        private int nextMessageIndex;

        public MessageFactory() {
            this.nextMessageIndex = 1;
        }

        public Message create() {
            return new Message("Message#" + (nextMessageIndex++));
        }
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                Message message = messageFactory.create();
                TimeUnit.SECONDS.sleep(5);
                messageBroker.produce(message);
                System.out.println("[PRODUCED]: " + message);
            } catch (InterruptedException exception) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
