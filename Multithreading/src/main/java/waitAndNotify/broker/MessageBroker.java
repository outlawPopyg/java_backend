package waitAndNotify.broker;

import waitAndNotify.model.Message;

import java.util.ArrayDeque;
import java.util.Queue;

public class MessageBroker {
    private final Queue<Message> messagesToBeConsumed;
    private final int maxStoredMessages;

    public MessageBroker(int maxStoredMessages) {
        this.messagesToBeConsumed = new ArrayDeque<>(maxStoredMessages);
        this.maxStoredMessages = maxStoredMessages;
    }

    public synchronized void produce(Message message) throws InterruptedException {

        while (messagesToBeConsumed.size() >= maxStoredMessages) {
            this.wait();
        }

        messagesToBeConsumed.add(message);
        this.notifyAll();
    }

    public synchronized Message consume() throws InterruptedException {

        while (messagesToBeConsumed.isEmpty()) {
            this.wait();
        }

        Message message = messagesToBeConsumed.poll();
        this.notifyAll();
        return message;
    }
}
