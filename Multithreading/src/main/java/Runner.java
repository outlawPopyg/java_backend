import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.IntStream;

public class Runner {
    public static void main(String[] args) throws InterruptedException {
        EvenNumberGenerator evenNumberGenerator = new EvenNumberGenerator();

        new Thread(() -> IntStream.range(0, 100).forEach(i -> System.out.println(evenNumberGenerator.generateEven())))
                .start();

        new Thread(() -> IntStream.range(0, 100).forEach(i -> System.out.println(evenNumberGenerator.generateEven())))
                .start();

        new Thread(() -> IntStream.range(0, 100).forEach(i -> System.out.println(evenNumberGenerator.generateEven())))
                .start();

        System.out.println(evenNumberGenerator.number);

    }

    private static class EvenNumberGenerator {
        private int number = -2;
        private Lock reentrantLock;

        public EvenNumberGenerator() {
            this.reentrantLock = new ReentrantLock();
        }

        public int generateEven() {
            reentrantLock.lock();
            try {
                return number += 2;
            } finally {
                reentrantLock.unlock();
            }
        }
    }

}
