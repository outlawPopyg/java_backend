package volatileExample;

import java.util.concurrent.TimeUnit;

public class Main {

    public static boolean isVisible = true;

    public static void main(String[] args) throws InterruptedException {
        new Thread(() -> {
            for (int i = 0; i < 100; i++) {
                if (isVisible) {
                    try {
                        TimeUnit.MILLISECONDS.sleep(1000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    System.out.println("Hello");
                }
            }
        }).start();

//        TimeUnit.SECONDS.sleep(2);

        new Thread(() -> {
            isVisible = false;
        }).start();
    }
}
