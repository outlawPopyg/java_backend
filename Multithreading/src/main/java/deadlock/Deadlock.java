package deadlock;


import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Deadlock implements Runnable {
    private final Lock firstLock;
    private final Lock secondLock;

    public Deadlock(Lock firstLock, Lock secondLock) {
        this.firstLock = firstLock;
        this.secondLock = secondLock;
    }

    public static void main(String[] args) {
        Lock firstLock = new ReentrantLock();
        Lock secondLock = new ReentrantLock();

        Thread firstThread = new Thread(new Deadlock(firstLock, secondLock));
        Thread secondThread = new Thread(new Deadlock(secondLock, firstLock));

        firstThread.start();
        secondThread.start();
    }

    @Override
    public void run() {
        String currentThreadName = Thread.currentThread().getName();

        System.out.println(currentThreadName + " is trying to lock firstLock: " + firstLock);
        firstLock.lock();
        try {
            System.out.println(currentThreadName + " is locked firstLock: " + firstLock);
            TimeUnit.SECONDS.sleep(1);

            System.out.println(currentThreadName + " is trying to lock secondLock: " + secondLock);
            secondLock.lock();
            try {
                System.out.println(currentThreadName + " is locked secondLock: " + firstLock);
            } finally {
                secondLock.unlock();
            }

        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        } finally {
            firstLock.unlock();
        }
    }
}
