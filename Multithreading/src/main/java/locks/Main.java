package locks;

import java.sql.Time;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        BoundedBuffer<Integer> boundedBuffer = new BoundedBuffer<>(5);

        new Thread(() -> {
            Stream.iterate(0, i -> i + 1).forEach(i -> {
                try {
                    boundedBuffer.put(i);
                    TimeUnit.SECONDS.sleep(1);

                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            });
        }).start();

        new Thread(() -> {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    boundedBuffer.take();
                    TimeUnit.SECONDS.sleep(5);

                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        }).start();

    }
}
