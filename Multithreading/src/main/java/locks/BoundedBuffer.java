package locks;

import java.util.Arrays;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class BoundedBuffer<T> {
    private final T[] elements;
    private final Lock lock;
    private final Condition fullCondition;
    private final Condition emptyCondition;
    private int size;

    @SuppressWarnings("unchecked")
    public BoundedBuffer(int capacity) {
        this.elements = (T[]) new Object[capacity];
        this.lock = new ReentrantLock();
        this.fullCondition = lock.newCondition();
        this.emptyCondition = lock.newCondition();
    }

    public boolean isFull() {
        lock.lock();
        try {
            return size == elements.length;
        } finally {
            lock.unlock();
        }
    }

    public boolean isEmpty() {
        lock.lock();
        try {
            return size == 0;
        } finally {
            lock.unlock();
        }
    }

    public void put(T element) {
        lock.lock();
        try {
            while (isFull()) {
                fullCondition.await();
            }

            elements[size] = element;
            System.out.println("[PUTTED]: " + Arrays.toString(elements));
            size++;
            emptyCondition.signalAll();

        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } finally {
            lock.unlock();
        }

    }

    public T take() {
        lock.lock();
        try {
            while (isEmpty()) {
                emptyCondition.await();
            }

            T result = elements[size - 1];
            elements[size - 1] = null;
            System.out.println("[TAKEN]: " + result + " " + Arrays.toString(elements));
            size--;
            fullCondition.signalAll();
            return result;
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        } finally {
            lock.unlock();
        }

    }

    @Override
    public String toString() {
        lock.lock();
        try {
            return "BoundedBuffer{" +
                    "elements=" + Arrays.toString(elements) +
                    ", size=" + size +
                    '}';
        } finally {
            lock.unlock();
        }
    }
}
