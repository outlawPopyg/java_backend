package org.outlaw.springcassandra.repository;
import org.outlaw.springcassandra.models.Taco;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface TacoRepository extends CassandraRepository<Taco, Taco.KeyClass> {}
