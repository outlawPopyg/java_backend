package org.outlaw.springcassandra.utils;

import lombok.experimental.UtilityClass;
import org.outlaw.springcassandra.models.Ingredient;
import org.outlaw.springcassandra.models.IngredientUDT;

@UtilityClass
public class TacoUDRUtils {

    public static IngredientUDT toIngredientUDT(Ingredient ingredient) {
        return IngredientUDT.builder()
                .name(ingredient.getName())
                .type(ingredient.getType())
                .build();
    }
}
