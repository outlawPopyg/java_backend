package org.outlaw.springcassandra.models;

import com.datastax.oss.driver.api.core.uuid.Uuids;
import lombok.Builder;
import lombok.Data;
import org.outlaw.springcassandra.utils.TacoUDRUtils;
import org.springframework.data.cassandra.core.cql.Ordering;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
@Table("tacos")
@Builder
public class Taco {

    @PrimaryKey
    private KeyClass id;

    @Data
    @PrimaryKeyClass
    public static class KeyClass {
        @PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED) // -> определение ключа раздела
        private UUID id;

        @PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED, // -> определение ключа кластеризации
                ordering = Ordering.DESCENDING)
        private String createdAt;
    }

    @NotNull
    @Size(min = 5, message = "Name must be at least 5 characters long")
    private String name;


    @Size(min = 1, message = "You should choose at least 1 ingredient")
    @Column("ingredients") // -> отображает список в столбец ingredients
    private List<IngredientUDT> ingredients; // UDT == User Defined Type

    public void addIngredient(Ingredient ingredient) {
        ingredients.add(TacoUDRUtils.toIngredientUDT(ingredient));
    }
}
