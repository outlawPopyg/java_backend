package org.outlaw.springcassandra.models;

import com.datastax.oss.driver.api.core.uuid.Uuids;
import lombok.Data;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;


/** В данном случае нас не волнует порядок хранения данных, поэтому свой-
ство id аннотировано лишь аннотацией @PrimaryKey, превращающей
это свойство в ключ раздела и ключ кластеризации с порядком следо-
вания по умолчанию. **/

@Data
@Table("orders")
public class TacoOrder implements Serializable {
    private static final long serialVersionUID = 1L;

    @PrimaryKey // -> определение первичного ключа
    private UUID id = Uuids.timeBased();

    private Date placedAt = new Date();

    @Column("tacos") // -> хранить список в столбце tacos
    private List<TacoUDT> tacos = new ArrayList<>();

    public void addTaco(TacoUDT tacoUDT) {
        tacos.add(tacoUDT);
    }
}
