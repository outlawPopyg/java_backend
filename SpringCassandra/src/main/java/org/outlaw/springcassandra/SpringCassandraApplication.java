package org.outlaw.springcassandra;

import org.outlaw.springcassandra.models.Ingredient;
import org.outlaw.springcassandra.models.Taco;
import org.outlaw.springcassandra.repository.IngredientRepository;
import org.outlaw.springcassandra.repository.TacoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@SpringBootApplication
public class SpringCassandraApplication {

    @Autowired
    IngredientRepository ingredientRepository;
    @Autowired
    TacoRepository tacoRepository;

    @Bean
    public CommandLineRunner runner2() {
        return args -> {

            Taco.KeyClass id = new Taco.KeyClass();
            id.setId(UUID.fromString("93e1c290-1aeb-4c94-92e7-866375b2464a"));
            id.setCreatedAt("2023-03-15T20:56:39.016669824Z");

            Taco taco = tacoRepository.findById(id).get();
            taco.addIngredient(new Ingredient("2", "Chicken", Ingredient.Type.PROTEIN));
            tacoRepository.save(taco);
        };
    }
    public static void main(String[] args) {
        SpringApplication.run(SpringCassandraApplication.class, args);
    }

}
