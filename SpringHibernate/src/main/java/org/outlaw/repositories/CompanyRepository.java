package org.outlaw.repositories;

import org.outlaw.models.Company;
import org.outlaw.models.Developer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;

@Repository
public class CompanyRepository {
    @PersistenceContext
    private EntityManager entityManager;
    private static final Logger logger = LoggerFactory.getLogger(CompanyRepository.class);

    @Transactional
    public void save(Company company) {
        entityManager.persist(company);
    }

    @Transactional
    public void saveDeveloper(Developer developer) {
        entityManager.persist(developer);
    }

    @Transactional
    public void printDeveloper() {
        Developer developer = entityManager.find(Developer.class, 2);
        Company company = entityManager.find(Company.class, 1);
        developer.setCompany(company);
        entityManager.flush();

        System.out.println(company.getDevelopers());
    }
}
