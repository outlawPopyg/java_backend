package org.outlaw.models;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.util.List;

@Entity
public class Company {
    @Id
    private int id;

    @Column
    private String name;

    @OneToMany(mappedBy = "company")
    private List<Developer> developers;

    public Company(int id, String name, List<Developer> developers) {
        this.id = id;
        this.name = name;
        this.developers = developers;
    }

    public Company(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Company() {}

    public List<Developer> getDevelopers() {
        return developers;
    }

    public void setDevelopers(List<Developer> developers) {
        this.developers = developers;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Company{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
