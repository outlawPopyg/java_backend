package org.outlaw.models;

import javax.persistence.*;
import java.util.List;

@Entity
public class Developer {
    @Id
    private int id;
    @Column
    private String name;

    @ManyToOne
    @JoinColumn(name = "company_id", referencedColumnName = "id")
    private Company company;

    public Developer(int id, String name, Company company) {
        this.id = id;
        this.name = name;
        this.company = company;
    }

    public Developer(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Developer() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Developer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", company=" + company +
                '}';
    }
}
