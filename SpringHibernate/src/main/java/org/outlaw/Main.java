package org.outlaw;

import org.outlaw.repositories.CompanyRepository;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext("org.outlaw");
        CompanyRepository repository = applicationContext.getBean(CompanyRepository.class);

        repository.printDeveloper();
    }
}
