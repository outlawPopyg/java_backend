# Java backend.

## Spring core.

- Бин - объект какого-то класса
- Контекст - набор бинов. Обращаясь к контексту - можем получить нужный бин.

### Конфигурация Spring - приложения

- .xml file
- Java annotations
- Java code

## Spring MVC

![dispatcher servlet](images/dispatcher_servlet.png)

### Ниже приведена последовательность событий, соотвествующая HTTP-запросу:

1. После получения HTTP-запроса `DispatcherServlet` обращается к интерфейсу `HandlerMapping`, который определяет, какой
   контроллер должен быть вызван, после чего, отправляет запрос в нужный контроллер.
2. Контроллер принимает запрос и вызывает соответсвующий служебный метод, основанный на GET или POST. Вызванный метод
   определяет данные Модели, основанные на определенной бизнес-логике и возвращает в `DispatcherServlet` имя `Вида` (
   View).
3. При помощи интерфейса `ViewResolver` `DispatcherServlet` определяет, какой `Вид` нужно использовать на основании
   полученного имени.
4. После того, как `Вид` (View) создан, `DispatcherServlet` отправляет данные `Модели` в виде аттрибутов в `Вид`,
   который в конечном итоге отображается отправляется клиенту через `response`.

Все вышеупомянутые компоненты, а именно, `HandlerMapping`, `Controller`, `ViewResolver`, являются частями
интерфейса `WebApplicationContext extends ApplicationContext`, с некторыми дополнительными оссобеностями, необходимыми
для создания web-приложений.

## Docker

Средство для автоматизации развертывания приложений. Чтобы автоматизировать развертывание и управление приложениями.

### Контейнерезация

Метод виртуализации, при котором ядро операционной системы поддерживает сразу несколько изолированных экзмепляров
пространства пользователя вместо одного. Работает на основе cgroups и пространства имен.

### Основные термины

- Docker-daemon - сервис, через который осуществляется все взаимодействие с контейнерами: создание и удаление, запуск
  и остановка.
- Docker-client - интерфейс командной строки для управления docker daemon.
- Docker-образ - файл, из которого разворачиваются контейнеры. Они хранятся в реестрах, например docker hub.
- Docker-container - развернутое из образа и работающее приложение.
- Dockerfile - файл с инструкциями для образа.
- Docker Compose - утилита для работы с многоконтейнерными приложениями. Намек на микросервисы.

### Команды

- `docker pull postgres` - install postgres
- `docker run --name nice-postgres -p 5432:5432 -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=outlaw -e POSTGRES_DB=test -d postgres`
  - запустить постгрес на порте 5432 и создать юзера с паролем и создать базу данных

### Пример dockerfile

```
 FROM openjdk:18-jdk-alpine.15 as builder (за основу образа берется openjdk 18)
 
 COPY . /software (внутрь образа копируется корневой каталог нашего приложения в папку software)
 
 WORKDIR /software (отмечаем в кач-ве рабочей директории папку)
 
 RUN apk add --update gradle
 RUN gradle bootJar

 FROM openjdk:12-alpine

 COPY --from=builder /software/build/libs/niceapp-0.0.1-SNAPSHOT.jar /opt/niceapp.jar

 ENTRYPOINT ["java", "-jar", "/opt/niceapp.jar"]
 CMD[""]
 
``` 

### Работа с контейнерами

* Сборка redis-образа с aof-функцией на основе Dockerfile:

```text
docker build -t redis-aof .
```

* Создание volume

```text
docker volume create --name redis_volume
```

* Запуск redis-контейнера

```text
docker run --name redis-aof-container -v redis_volume:/data -p 6380:6380 -d redis-aof
```

## Spring Boot

Это фреймворк построенный над Spring, который представляет широкие возможности автоконфигурирования и улучшает
возможности
конфигурирования в целом

- Tomcat - представляет собой Java-приложение, которое заботится об открытии порта для взаимодействия с клиентом,
  настройке сессий, кол-ва запросов, длине заголовка и еще многих операциях.
- аннотация `@SpringBootApplication` включает в себя `@Configuration`, `@ComponentScan`, `@EnableAutoConfiguration`.

* `application.properties`
  Файл с параметрами компонентов приложения. Большое кол-во параметров уже используется в автоматическом режиме.
  Например, `spring.datasource.*` используются для настройки БД
* `CommandLineRunner` - интерфейс, реализация которого (если она объявлена как бин) автоматически подхватывается
  и используется как логика работы после запуска.

## Разбор принципов работы Spring Boot на примере настроек БД

**Стартер** - обычный pom.xml с нужными зависим-ми.

* `spring-boot-starter-data-jpa` - спец стартер для настроек работы через Spring Data JPA
    * `spring-boot-starter-jdbc` - спец стартер для настроек работы через JDBC
        * `spring-boot-starter` - Содержит необходимые зависимости, необходимые для работы Spring Boot
            * `spring-boot-autoconfigure` - механизмы автоконфигурации
        * `HikariCP` - Hikari connection pool
        * `spring-jdbc` - JdbcTemplate, NamedParameterJdbcTemplate, Mapper, ...
    * `jakarta.transaction-api` - JTA
    * `jakarta.persitence-api` - JPA
    * `hibernate-core` - Hibernate
    * `spring-data-jpa` - Jpa repository

### `spring.factories`

Это файл из `spring-boot-autoconfigure`, в котором перечислены классы для автоконфигурации.
Например, `org.springfraemwork.boot.autoconfigure.jdbc.DataSourceAutoConfiguration`

* `DataSourceConfiguration` - класс, который позволяет настроить `DataSource`, как он понимает какой тип `DataSource`
  использовать ?
    * `@ConditionalOnClass(HikariDataSource.class)` - данная аннтотация говорит, что если в `classpath`
      есть `HikariDataSource`, то нужно активировать бин
      из класса, которым помечена эта аннотация. (classpath — это просто набор путей, по которым компилятор Java и JVM
      должны находить необходимые классы для компиляции или выполнения других классов.)
* Почему `DataSourceConfiguration` вообще отрабатывает ?
    * `@Import({DataSourceConfiguration.Hikari.class, ...` - данная аннотация комбинирует конфигурацию
      из `DataSourceAutoConfiguration` и `Hikari` из `DataSourceConfiguration` класса
* Почему он забирает свойства из `application.properties` ?
    * ```
     @ConfigurationProperties(prefix = "spring.datasource")
     public class DataSourceProperties
     ```
      Аннотация `ConfigurationProperties` говорит, что все свойства, которые начинаются с `spring.datasource` нужно
      вставить в поля класса `DataSourceProperties`

### Итого

```text
 spring-boot-autoconfigure -> spring.factories -> DataSourceAutoConfiguration -> DataSourceProperties -> DataSourceConfiguration
```

## Написание собственного стартера

Я написал библиотеку `JdbcLibrar`, у которой есть метод, выводящий все таблицы из базы.  
Из зависсимостей у нее есть только драйвер.

```xml

<dependencies>
    <dependency>
        <groupId>org.postgresql</groupId>
        <artifactId>postgresql</artifactId>
        <version>42.2.27</version>
    </dependency>
</dependencies>
```

Я отдельно вынес класс с пропертями для удобства.

```java
public class DatabaseProperties {
    private String username;
    private String password;
    private String url;

    public DatabaseProperties(String username, String password, String url) {
        this.username = username;
        this.password = password;
        this.url = url;
    }

    // геттеры ...
}
```

Так же отдельно вынес класс с подключением к бд. У этого класса есть поле с пропертями

```java
public class DatabaseConnection {
    private final DatabaseProperties properties;

    public DatabaseConnection(DatabaseProperties properties) {
        this.properties = properties;
    }

    public Connection getConnection() {
        try {
            return DriverManager.getConnection(
                    properties.getUrl(),
                    properties.getUsername(),
                    properties.getPassword());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
```

Собственно сам класс библиотеки.

```java
public class JdbcLibrary implements SomeService {

    private final Connection connection;

    public JdbcLibrary(Connection connection) {
        this.connection = connection;
    }

    @Override
    public List<String> getAllTableNames() {
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from information_schema.tables limit 10");
            List<String> list = new LinkedList<>();

            while (resultSet.next()) {
                list.add(resultSet.getString("table_name"));
            }

            return list;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }
}
```

После того как я написал код, я выполнил команду `maven install` и загрузил проект в папку `.m2`

#### Собственно, зачем мне писать стартер ?

Допустим, я хочу использовать эту библиотеку в стороннем проекте, например, в Spring Boot. Без стартера, мне пришлось бы
конфигурировать бины
`DatabaseProperties`, `DatabaseConnection`, `JdbcLibrary` вручную. Причем всякий раз, когда эта библиотека будет
перекачевывать из проекта в проект, мне постоянно
придется писать один и тот же конфигурационный код. Стартер же предлагает мне написать конфигурацию всего один раз и при
надобности предлагает возможность переопределить
какие-то бины.

Зависсимости моего стартера `spring-starter-awesome-jdbs`. В них входит моя библиотека `JdbcLibrary`

```xml

<dependencies>

    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter</artifactId>
    </dependency>

    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-configuration-processor</artifactId>
    </dependency>

    <dependency>
        <groupId>org.projectlombok</groupId>
        <artifactId>lombok</artifactId>
        <optional>true</optional>
    </dependency>

    <dependency>
        <groupId>org.outlaw</groupId>
        <artifactId>JdbcLibrary</artifactId>
        <version>1.0-SNAPSHOT</version>
    </dependency>
</dependencies>
```

Для стартера понадобяться 2 класса: класс с пропертями и класс с самой автоконфигурацией, а так же `spring.factories`.  
`spring.factories` должен располагаться по следующему пути `resources/META-INF/spring.factories`.  
Вот, что в нем лежит

```text
org.springframework.boot.autoconfigure.EnableAutoConfiguration=\
  org.outlaw.springawesomestarterjdbc.JdbcLibraryAutoConfiguration
```

Мы кладем туда все автоконфигурации для нашего стартера.

В класс ниже есть аннотация `@ConfigurationProperties` с прфиком. Это занчит что спринг заберет
из `application.properties` Спринг Бут проекта в котором
будет исопльзоваться этот стартер все проперти с префиксом `jdbc-library` и с основной частью, соответствующей названиям
переменным и присвоит им заначения.

```java

@ConfigurationProperties(prefix = "jdbc-library")
public class JdbcLibraryProperties {
    private String username;
    private String password;
    private String url;

    // getters, setters, ...
}
```

Это класс с автоконфигурацией. Аннотация `@ConditionalOnClass(JdbcLibrary.class)` не запустит этот класс, если в
classpath
отстутствуте `JdbcLibrary`. `@EnableConfigurationProperties(JdbcLibraryProperties.class)` подтянет проперти файлы из
предыдущего класса.
В этом классе мы просто конфигурируем нашу библиотеку.

```java

@Configuration
@EnableConfigurationProperties(JdbcLibraryProperties.class)
@ConditionalOnClass(JdbcLibrary.class)
public class JdbcLibraryAutoConfiguration {
    private final JdbcLibraryProperties properties;

    public JdbcLibraryAutoConfiguration(JdbcLibraryProperties properties) {
        this.properties = properties;
    }

    @Bean
    @ConditionalOnMissingBean
    public DatabaseProperties databaseProperties() {
        return new DatabaseProperties(properties.getUsername(), properties.getPassword(), properties.getUrl());
    }

    @Bean
    @ConditionalOnMissingBean
    public DatabaseConnection databaseConnection(DatabaseProperties databaseProperties) {
        return new DatabaseConnection(databaseProperties);
    }

    @Bean
    @ConditionalOnMissingBean
    public JdbcLibrary jdbcLibrary(DatabaseConnection databaseConnection) {
        return new JdbcLibrary(databaseConnection.getConnection());
    }
}
```

Анноатции `@ConditionalOnMissingBean` не будут загружаться в BeanFactory если аннотация такого типа там уже есть.
Делается это для того, чтобы пользователи
нашего стартера могли переопределить какие-то бины, например, поменять проперти. Об этом чуть позже.

Опять же вызываем `maven install`.

Создаем пустой Spring Boot проект и добавляем в зависсимость наш стартер.
`application.properties`:

```properties
jdbc-library.username=postgres
jdbc-library.password=outlaw
jdbc-library.url=jdbc:postgresql://localhost:5432/javabackend
```

Точка входа в приложение.

```java

@SpringBootApplication
public class StarterDemoApplication {

    @Bean
    public CommandLineRunner runner(JdbcLibrary jdbcLibrary) {
        return args -> {
            System.out.println(jdbcLibrary.getAllTableNames());
        };
    }

    public static void main(String[] args) {
        SpringApplication.run(StarterDemoApplication.class, args);
    }
}
```

Теперь, допустим, мы хотим поменять базу данных. Для этого создаем в Spring Boot проекте класс конфигурации и
переопределяем бин с пропертями.

```java

@Configuration
public class AppConfiguration {
    @Bean
    public DatabaseProperties databaseProperties() {
        return new DatabaseProperties("postgres", "outlaw", "jdbc:postgresql://localhost:5432/postgres");
    }
}
```

То есть мы вклинились в работу стартера и заранее задали класс с проперятми, и теперь когда стартер дойдет до бина с
этими пропертями он ничего не будет
делать так как там аннотация `@ConditionalOnMisingBean`.

## JPA

* Java Persistence API - реализация ORM в Java.
* `EntityManager` - менеждер сущностей, интерфейс JPA, отвечает за манипуляции с сущностями.

### @OneToMany, @ManyToOne

В примере ниже есть две `@Entity` сущности: `Developer` и `Company`
Они соединены связями `many-to-one`. Причем owning на стороне `Developer`.
Мы выполняем `developer.setCompany(company)` и хотим посмотреть добавился ли этот developer
в список developer'ов компании company. (`company.getDevelopers()`) без вызова `entityManager.flush()`
в список company ничего бы не добавилось, хотя sql-код был выполнен и была вставка в таблицу.

```java
@Transactional
public void printDeveloper(){
        Developer developer=entityManager.find(Developer.class,2);
        Company company=entityManager.find(Company.class,1);
        developer.setCompany(company);
        entityManager.flush();

        System.out.println(company.getDevelopers());
        }
```

## Hibernate.

- ORM - концепция о том, что Java объекты можно представить как данные в БД (и наоборот). Она нашла воплощение в виде
  спецификации JPA.
- Спецификация - это уже описание Java API, которое выражает эту концепцию. Спецификация рассказывает, какими средствами
  мы должны быть обеспечены (т.е через какие интерфейсы мы сможем работать), чтобы работать по концепции ORM. И как
  использовать эти средства.
- Реализацию спецификация не описывает. Реализацию JPA еще называют JPA Provider. Одной из самых известных реализаций
  JPA является Hibernate.

## Уровни изоляции.

### ACID

- **Атомарность** (все или ничего)
- **Консистентность** (Согласованность) - Транзакция, достигашая своего нормального завершения, фиксирует свои только
  допустимые результаты. Это
  условие является необходимым для четвертого св-ва.
- **Изолированность** - во время выполнения транзакции паралелльные транзакции не должны оказывать влияние на ее
  результат.
- **Долговечность** - выполненная транзакция гарантирует, что данные сохранены и при последующих сбоях эта ин-ия никуда
  не денется.

### Блокировки

Механизм, позволяющий вести паралелльную работу с одними и теми же данными в бд. Когда более чем одна транзакция хочет
получить доступ к данным
в одно и то же время. В силу вступают блокировки, гарантирующие, что только одна транзакция изменит данные.

Существуют два типа блокировок: оптимистичный и пессимистичный.

- **Оптимистичный** подразумевает, что паралелльно выполняющиеся транзакции редко обращаются к одним и тем же данным и
  позволяет им спокойно выполнять
  любые чтения и обновления данных.
- **Пессимистичный**, напротив, ориентирован на транзакции, которые часто конкурируют за одни и те же данные и поэтому
  блокирует доступ к данным,
  в тот момент когда читает их.

### Изоляции

- **Грязное чтение** - есть две транзакции, одна изменяет данные но не комитит, но вторая видит эти изменения.
- **Неповторяемое чтение** - транзакция, при выполнении последовательных чтений объекта читает изменения, внесенные уже
  другими
  , подтвержденными транзакциями. То есть, если одна транзакция прочитала сущность, потом другая транзакция ее обновила
  и успешно
  завершилась, то первая транзакция, если еще раз прочитает, получит уже обновленные данные.
- **Фантомное чтение** - то же самое, что и неповторяемое, только связано с добавлением новой записи.

### Уровни изоляции транзакций

- **Serializable** - транзакции полностью изолируются друг от друга и ни одна транзакция никоим образом не влияет на
  другие.
  Сама низкая степень паралелльности.
- **Repeatable read** - исключение грязного чтения и неповторяющегося чтения. Блокировка обрабатываемых строк не
  допускает
  их изменение другими транзакциями но разрешает добавление новых записей => возникает эффект фантоиного чтения.
- **Read committed** - исключение грязного чтения, но другим транзакциям разрешено изменять заблокированные данные.
- **Read uncommitted** - гарантирует только то, что изменения, внесенные одной транзакцией, не будут перезаписаны
  другой.

### Распространение транзакций

Если один метод с @Transactional вызовет другой метод @Transactional что делать ? Создать новую транзакция или все
выполнить в старой ?

- Propagation.REQUIRED - применятся по умолчанию. При входе в @Transactional метод будет использовать уже сущ.
  транзакцию.
- Propagation.REQUIRES_NEW - транзакция всегда создается при входе в метод с этой аннотацией, ранее созданные транзакции
  приостанавливаются
  до возврата из метода.
- Propagation.MANDATORY - подразумевает, что транзакция уже создана, если такой нет, то кидается исключение.

## AOP

Если есть функционал, который не получится вынести в отдельный модуль и даже если получится, впоследствии его всюду
подключать не очень то и удобно.

* Совет (advice) - дополнительная логика, код, который вызывается из точки соединения.
* Точка соединения (join point) - точка в выполняемой программе (вызов метода, создание объекта, обращение переменной),
  где следует применить совет. Иначе говоря, это некоторое регулярное выражение, с помощью которого и находятся места
  для
  внедрения кода (места для применения советов).
* Срез (pointcut) - набор точек соедиения. Срез определяет, подходит ли данная точка соединения к данному совету.
* Аспект (aspect) - модуль или класс, реализующий сквозную функциональность. Аспект изменяеи поведение остального кода,
  применяя совет в точках соединения, определенных некоторым срезом. Иными словами, это комбинация советов и точек
  соединения.
* Внедрение (introduction) - изменения структуры класса и/или изменение иерархии наследования для добавления
  функциональности аспекта в инородный код.
* Цель (target) - объект, к которому будут применяться советы.

### Типы advice'ов

* `Before` - советы данного типа запускаются перед выполнением join point'ов.
* `After` - - советы данного типа запускаются после выполнения join point'ов, как в обычных случаях, так и при бросании
  исключений.
* `After Returning` - данные советы запускаются только тогда, когда join point отработал без ошибок.
* `After Throwing` - данный вид советов предназначен для тех случаев, когда метод, то есть точка соединения выдает
  исключение. Мы можем использовать этот совет для некой обработки неудачного выполнения ( к примеру, для отката всей
  транзакции или логирования с необходимым уровнем трассировки)
* `Around` - совет, который окружает join point. С его помощью мы можем, к примеру, выбрать, выполнять данный метод
  join point'а или нет. Можно написать код совета, который будет выполняться до и после выполнения метода join point'а.
  В обязанности around advice входит вызов метода точки соединения и возвращение значений, если метод что-то возвращает.
  То есть в этом совете можно по просту сымитировать работу целевого метода, не вызывая его, и в качестве результата,
  вернуть что-то свое.

### Pointcut designators

#### Within

| Explanation                                                                                                                              | Pattern                                               |
|------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------|
| Match with any joinpoint within `business` package.                                                                                      |    `within(com.jbd.business.*)`                         |
| Match with any joinpoint within `business` and its sub-packages                                                                          | `within(com.jbd.business..*)`                         |
| Match with any join point (method) execution where the declared type of the target object (the class) has an `@RestController` annotation. | `@within(org.springframework.web.bind.RestController)` |

#### Execution

| Explanation                                                                        | Pattern                                                          |
|------------------------------------------------------------------------------------|------------------------------------------------------------------|
| To match with the execution of any public method                                   | `execution(public * *(..))`                                      |
| The match with any setter                                                             | `execution(* set*(..))`                                          |
| Match with any method inside `ActorRepository`, defined inside a particular package | `execution(* c.jbd.saop.gettingstarted.dao.ActorRepository.*(..)` |
| Match with any DAO `delete()` method with any type of arguments.                   | `execution(* c.jbd.app.dao.*.*(..))`                             |
| Match any method inside any DAO with exactly one argument of any type.             | `execution(* c.jbd.saop.gettingstarted.dao.*.*(*))`              |

## Cache

- `@Cacheable` - кэширование возвращаемого результата для определенных входных параметров.

```java
@Override
@Cacheable(value = "users", key = "#name")
public User create(String name,String email){
        return repository.save(new User(name,email));
        }
```

`#name - ключ кэширования`

- `@CachePut` - пропускает приложение в метод, при этом, обновляя кэш для возвращаемого значения, даже если оно
  уже закешировано.
- `@CacheEvict` - Иногда возникает необходимость жёстко обновить какие-то данные в кэше. Например, сущность
  уже удалена из базы, но она по-прежнему доступна из кэша. Для сохранения консистентности данных, нам необходимо
  хотя бы не хранить в кэше удалённые данные.
- `@Caching` - группировка настроек.

```java
@Caching(
        cacheable = {
                @Cacheable("users"),
                @Cacheable("contacts")
        },
        put = {
                @CachePut("tables"),
                @CachePut("chairs"),
                @CachePut(value = "meals", key = "#user.email")
        },
        evict = {
                @CacheEvict(value = "services", key = "#user.name")
        }
)
    void cacheExample(User user){
            }
```

- `CacheManager`
  ![](images/cachemanager.png)
  Тут можно увидеть в каком виде хранятся кэш. Так же можно написать свой кастомный `CacheManager`.

```java
@Bean("habrCacheManager")
public CacheManager cacheManager(){
        return new ConcurrentMapCacheManager(){
@Override
protected Cache createConcurrentMapCache(String name){
        return new ConcurrentMapCache(
        name,
        CacheBuilder.newBuilder()
        .expireAfterWrite(1,TimeUnit.SECONDS)
        .build().asMap(),
        false
        );
        }
        };
        }
```

Здесь указывается время жизни кэша, после того, как он будет записан в мапу. В данном случае - это 1 секунда.

## Spring Validation

- `@NotEmpty` - проверяет строку на непустоту, при этом она может быть `null`
- `@NotBlank` - делает то же, что и `@NotEmpty`, но запрещает строке быть `null`
- `@Pattern` - проверяет строку на матчинг с регуляркой
- `@Valid` - валидирует

```java
public class UserRequest {
    private String name;

    private String login;

    @NotBlank(message = "password is null")
    private String password;

    private Instant birthday;

}
```

```java
@PostMapping("/users")
public ResponseEntity<?> addUser(@RequestBody @Valid UserRequest userRequest){
        log.info("Creating user");
        return ResponseEntity.ok("User is valid");
        }
```

Если `userRequest` не пройдет валидацию, то выполнится дефолтный `ExceptionHandler` который выкинет 404 ошибку.

### Написание кастомного валидатора

```java

@Documented
@Constraint(validatedBy = {UserLoginAndNameValidator.class})
@Target({ElementType.TYPE, PARAMETER, FIELD})
@Retention(RUNTIME)
public @interface UserConstraint {
    String message() default "{UserConstraint error}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
```

Кастомная аннотация, которая будет обрабатываться в `UserLoginAndNameValidator`.

```java
public class UserLoginAndNameValidator implements ConstraintValidator<UserConstraint, UserRequest> {

    private static final String MESSAGE = "User must have only login or name.";

    @Override
    public boolean isValid(UserRequest request, ConstraintValidatorContext context) {
        if (Objects.nonNull(request.getName()) && Objects.nonNull(request.getLogin())) {

            buildConstraintViolationWithTemplate(context, "name");
            return false;
        }

        return true;
    }

    private void buildConstraintViolationWithTemplate(ConstraintValidatorContext context, String fieldName) {
        context.buildConstraintViolationWithTemplate(MESSAGE)
                .addPropertyNode(fieldName)
                .addConstraintViolation()
                .disableDefaultConstraintViolation();
    }

}
```

Класс который вадидирует сущности, помеченные аннотацией `@UserConstraint`.  
Здесь переопределяется метод `isValid`, который говорит прошел ли объект валидацию или нет.  
Метод `buildConstraintViolationWithTemplate` генерирует констрейнт, который впоследствии будет перехвачен в
`GlobalExceptionHandler`.

```java

@Getter
@Builder
public class ResponseErrorMessage {
    private final long timestamp = Instant.now().getEpochSecond();
    private final List<Error> errors;

    @AllArgsConstructor
    @Getter
    public static class Error {
        private final String fieldName;
        private final String code;
        private final String detailMessage;
    }
}
```

```java

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public final ResponseEntity<ExceptionMessage> handleHttpMessageNotReadableException(HttpMessageNotReadableException ex) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(ExceptionMessage.builder()
                        .message(ex.getMessage())
                        .exceptionName(ex.getClass().getSimpleName())
                        .build());
    }


    @ExceptionHandler(MethodArgumentNotValidException.class)
    public final ResponseEntity<ResponseErrorMessage> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(ResponseErrorMessage.builder()
                        .errors(ex.getBindingResult().getFieldErrors().stream()
                                .map(fieldError -> new ResponseErrorMessage.Error(fieldError.getField(), fieldError.getCode(), fieldError.getDefaultMessage()))
                                .collect(Collectors.toList()))
                        .build());
    }

}
```

### Тестирование

`POST localhost:8080/users { "name": "Kalim", "login": "outlaw" }`

```json
{
  "timestamp": 1677865634,
  "errors": [
    {
      "fieldName": "password",
      "code": "NotBlank",
      "detailMessage": "password is null"
    },
    {
      "fieldName": "name",
      "code": "UserConstraint",
      "detailMessage": "User must have only login or name."
    }
  ]
}
```

## JOOQ ORM

### CRUD

```java
public Country insertValues(Country country){
        return dsl.insertInto(Countries.COUNTRIES)  //insert into countries
        .values(country.getId(),country.getName(),country.getPopulation(),nameOrNull(country.getGovernmentForm()))  // values (? ? ? ?)
        .returning()  // returning
        .fetchOne()  // *
        .into(Country.class);
        }
```

Аналог для `repository.save(country)` в JOOQ

```java
public Country insert(Country country){
        return dsl.insertInto(Countries.COUNTRIES)
        .set(dsl.newRecord(Countries.COUNTRIES,country))  // мапим модель в сущность
        .returning()
        .fetchOptional()
        .orElseThrow(()->new DataAccessException("Error inserting entity: "+country.getId()))
        .into(Country.class);
        }
```

Как мы заметили, данный запрос возвращает сущность целиком.
Вы можете определить, что именно Вам нужно вернуть в методе fetch(). Выглядит это так:

```java
public Long insertAndReturnId(Country country){
        return dsl.insertInto(Countries.COUNTRIES)
        .set(dsl.newRecord(Countries.COUNTRIES,country))
        .returning(Countries.COUNTRIES.ID) //в возвращаемом Record только одно значение - id
        .fetchOptional()
        .orElseThrow(()->new DataAccessException("Error inserting entity: "+country.getId()))
        .get(Countries.COUNTRIES.ID); //достаём id
        }
```

## Spring Security

- `Authentication` - объект, который хранит для каждого запроса информацию о пользователе (доверителе) и статус его
  аутентификации.
- `SecurityContext` - информация о безопасности, которая ассоциирована с текущим потоком исполнения. Хранит в себе
  `Authentication`.
- `SecurityContextHolder` - привязывает `SecurityContext` к текущему потоку исполнения. По умолчанию - `ThreadLocal`.

### Компоненты Spring Security

#### DelegatingFilterProxy

Поскольку фильтры являются частью `Servlet API` -> у них нет доступа к спрингвскому `ApplicationContext`.  
Чтобы исправить эту проблему и сделать некоторые фильтры бинами, при запуске приложения создается экземпляр
класса-фильтра `DelegatingFilterProxy`, он делегирует обработку фильтру-бину, название которого указывается в поле
`targetBeanName`.

```java
package org.springframework.web.filter;

public class DelegatingFilterProxy extends GenericFilterBean {

    @Nullable
    private WebApplicationContext webApplicationContext; // Spring-контекст приложения

    @Nullable
    private String targetBeanName; // имя бина-фильтра, которому делегируется обработка запроса

    @Nullable
    private volatile Filter delegate; // бин-фильтр, которому делегируется обработка запроса

    public DelegatingFilterProxy(String targetBeanName, @Nullable WebApplicationContext wac) {
        // ...

        this.setTargetBeanName(targetBeanName);
        this.webApplicationContext = wac;

        // ...
    }

    // метод инициализации фильтра-бина для делегирования
    protected Filter initDelegate(WebApplicationContext wac) throws ServletException {
        String targetBeanName = getTargetBeanName();
        // ...
        Filter delegate = wac.getBean(targetBeanName, Filter.class);
        // ...
        return delegate;
    }

    // метод делегирования запроса бину с названием targetBeanName
    protected void invokeDelegate(
            Filter delegate, ServletRequest request, ServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        delegate.doFilter(request, response, filterChain);
    }

    // ...
}

```

В свою очередь, `DelegatingFilterProxy` создается на основе компонента `DelegatingFilterProxyRegistrationBean` в классе
автоконфигурации `SpringBoot`:

```java
package org.springframework.boot.autoconfigure.security.servlet;

// ...
@Configuration
public class SecurityFilterAutoConfiguration {

    // ...

    @Bean
    @ConditionalOnBean(
            name = {"springSecurityFilterChain"}
    )
    public DelegatingFilterProxyRegistrationBean securityFilterChainRegistration(SecurityProperties securityProperties) {
        DelegatingFilterProxyRegistrationBean registration = new DelegatingFilterProxyRegistrationBean("springSecurityFilterChain", new ServletRegistrationBean[0]);
        registration.setOrder(securityProperties.getFilter().getOrder());
        registration.setDispatcherTypes(this.getDispatcherTypes(securityProperties));
        return registration;
    }

// ...
```

Данный компонент содержит метод для получения объекта `DelegatingFilterProxy` - `getFilter()`.

```java
package org.springframework.boot.web.servlet;

public class DelegatingFilterProxyRegistrationBean {
    @Override
    public DelegatingFilterProxy getFilter() {
        return new DelegatingFilterProxy(this.targetBeanName, getWebApplicationContext()) {
            // ...
        };
    }
}
```

Именно метод `getFilter()` вызывается механизмом конфигурации Spring для добавления `DelegatingFilterProxy` в
`ApplicationContext`.

#### FilterChainProxy

В части инфраструктуры Spring Security `DelegatingFilterProxy` первым принимает запрос и направляет его цепочке фильтров
безопасности (бину со значением, которое указано в `targetBeanName` ). В качестве `targetBeanName` как правило
указывается бин `springSecurityFilterChain`.

Ниже приведен код инициализации бина `springSecurityFilterChain` в классе конфигурации Spring:

```java
package org.springframework.security.config.annotation.web.configuration;

@Configuration
public class WebSecurityConfiguration implements ImportAware, BeanClassLoaderAware {
    // ...

    @Bean
    public Filter springSecurityFilterChain() throws Exception {
        // ...
    }

// ...
```

Что же представляет из себя бин `springSecurityFilterChain` ? В качестве реализации этого бина используется класс
`FilterChainProxy`, содержащий фильтры для обеспечения безопасности:
![](images/security-filters.png)

Для каждого запроса создается "виртуальная цепочка фильтров" (Spring Security Filters), внутри которых происходит
проверка безопасности запроса. При этом, каждый из фильтров имеет возможность "заблокировать запрос", если он не
соответствует критериям безопасности. Также, поскольку `FilterChainProxy` является бином, каждый из фильртов цепочки
имеет доступ к контексту Spring.

Создание виртуальной цепочки фильтров:

```java
package org.springframework.security.web;

public class FilterChainProxy extends GenericFilterBean {

    private void doFilterInternal(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        FirewalledRequest firewallRequest = this.firewall.getFirewalledRequest((HttpServletRequest) request);
        HttpServletResponse firewallResponse = this.firewall.getFirewalledResponse((HttpServletResponse) response);
        // ...
        VirtualFilterChain virtualFilterChain = new VirtualFilterChain(firewallRequest, chain, filters);
        virtualFilterChain.doFilter(firewallRequest, firewallResponse);
    }
}
```

В свою очередь,`VirtualFilterChain`, как вложенный класс `FilterChainProxy`, содержит список фильтров, которые
необходимо применить к текущему URL:

```java
private static final class VirtualFilterChain implements FilterChain {

    // ...

    private final List<Filter> additionalFilters; // список внутренних фильтров Spring Security

    private final int size; // количество внутренних фильтров

    private int currentPosition = 0; // номер текущего фильтра, выполняющего обработку запроса

    @Override
    public void doFilter(ServletRequest request, ServletResponse response) throws IOException, ServletException {
        if (this.currentPosition == this.size) {
            // ...
            this.currentPosition++;
            // получение текущего фильтра
            Filter nextFilter = this.additionalFilters.get(this.currentPosition - 1);
            // ...
            // отправка запроса текущему фильтру, в качестве цепочки фильтров передается текущая виртуальная цепочка
            nextFilter.doFilter(request, response, this);
        }
    }
}
```

Рассмотрим подробнее некоторые из фильтров `VirtualChain`, отвечающие за безопасность запросов.

#### SecurityContextPersistenceFilter

Данный фильтр отвечает за работу с `SecurityContext` между запросами. Задачи - получить `SecurityContext` для текущего
запроса, положить его в `SecurityContextHolder`, очистить `SecurityContextHolder` после выполения запроса и сохранить
изменения контекста.

```java
package org.springframework.security.web.context;

public class SecurityContextPersistenceFilter extends GenericFilterBean {

    private SecurityContextRepository repo; // хранилище для SecurityContext


    private void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        // ...
        HttpRequestResponseHolder holder = new HttpRequestResponseHolder(request, response);
        // получаем SecurityContext из хранилища до того, как будет выполнен запрос
        SecurityContext contextBeforeChainExecution = this.repo.loadContext(holder);
        try {
            // привязываем текущий контекст безопасности к потоку исполнения
            SecurityContextHolder.setContext(contextBeforeChainExecution);
            // ...
            // передаем запрос на обработку дальше
            chain.doFilter(holder.getRequest(), holder.getResponse());
        } finally {
            // после завершения запроса получаем контекст
            SecurityContext contextAfterChainExecution = SecurityContextHolder.getContext();
            // очищаем текущий поток исполнения от контекста безопасности
            SecurityContextHolder.clearContext();
            // сохраняем актуальный контекст безопасности в хранилище
            this.repo.saveContext(contextAfterChainExecution, holder.getRequest(), holder.getResponse());
            // ...
        }
    }
}
```

В качестве реализации `SecurityContextRepository` по умолчанию используется `HttpSessionSecurityContextRepository`.

#### CSRF Filter

Данный фильтр отвечает за создание и проверку csrf-токенов.

```java
package org.springframework.security.web.csrf;

public final class CsrfFilter extends OncePerRequestFilter {
    // ...

    private final CsrfTokenRepository tokenRepository; // компонент, отвечающий за хранение и создание токенов

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        // загружаем существующий токен
        CsrfToken csrfToken = this.tokenRepository.loadToken(request);
        boolean missingToken = (csrfToken == null);
        // если токен отстутствует
        if (missingToken) {
            // создаем новый
            csrfToken = this.tokenRepository.generateToken(request);
            // сохраняем его для текущего запроса-ответа
            this.tokenRepository.saveToken(csrfToken, request, response);
        }
        // кладем информацию о csrf-токене как атрибут текущего запроса
        request.setAttribute(CsrfToken.class.getName(), csrfToken);
        request.setAttribute(csrfToken.getParameterName(), csrfToken);
        // нет смысла проверять не POST/PUT запросы
        if (!this.requireCsrfProtectionMatcher.matches(request)) {
            // ...
            filterChain.doFilter(request, response);
            return;
        }
        // для остальных запросов получаем токен из заголовка, либо из параметра запроса
        String actualToken = request.getHeader(csrfToken.getHeaderName());
        if (actualToken == null) {
            actualToken = request.getParameter(csrfToken.getParameterName());
        }
        // если токен неверный 
        if (!csrfToken.getToken().equals(actualToken)) {
            // создаем исключение и прерываем работу фильтров
            AccessDeniedException exception = (!missingToken) ? new InvalidCsrfTokenException(csrfToken, actualToken)
                    : new MissingCsrfTokenException(actualToken);
            this.accessDeniedHandler.handle(request, response, exception);
            return;
        }
        filterChain.doFilter(request, response);
    }

}
```

#### LogoutFilter

Данный фильтр отвечает за выход пользователя из системы.

```java
package org.springframework.security.web.authentication.logout;

public class LogoutFilter extends GenericFilterBean {
    // ...

    private RequestMatcher logoutRequestMatcher; // компонент, отвечающий за матчинг URL для выхода, например, URL должен соответствовать /lougout

    private final LogoutHandler handler; // компонент, содержащий логику, которую необходимо выполнить при выходе пользователя из системы

    private final LogoutSuccessHandler logoutSuccessHandler; // компонент, содержащий логику, которую необходимо выполнить при УСПЕШНОМ выходе пользователя из системы

    private void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        if (requiresLogout(request, response)) { //если запрос соответствует необходимому URL (проверяется через logoutRequestMatcher)
            Authentication auth = SecurityContextHolder.getContext().getAuthentication(); // получение объекта текущей аутентификации
            // ...
            this.handler.logout(request, response, auth); // вызов необходимых обработчиков выхода, например - очистка cookies, csrf, remeber-me и.д.
            this.logoutSuccessHandler.onLogoutSuccess(request, response, auth); // вызов обработчика успешного выхода, например SimpleUrlLogoutSuccessHandler - переход на URL /signIn после выхода
            // завершение выполнения запроса
            return;
        }
        // передача запроса остальным фильтрам
        chain.doFilter(request, response);
    }
```

Обработчики `LogoutFilter`:
![](images/logout_handlers.png)

### Authentication

Мы рассмотрели компоненты Spring Security, работающие в момент отправки запроса "залогиненным" пользователем. Теперь
рассмотрим случай, когда пользователь вводит логин и пароль и для него требуется аутентификация.  
В очереде `VirtualFilterChain` следующим после `LogoutFilter` идет `UsernamePasswordAutentificationFilter`, задачей
которого является аутентификация пользователя. Данный фильтр отрабатывает в случае стандартной Spring Security в
Spring Boot. Внутри `UsernamePasswordAutentificationFilter` создается объект-имплементация `Autentification` -
`UsernamePasswordAutentificationToken`, включающий логин и пароль пользователя. После чего объект направляется
компоненту `AutentificationManager`, выполняющему проверку данных пользователя.

```java
public class UsernamePasswordAuthenticationFilter extends AbstractAuthenticationProcessingFilter {
    // ...

    // метод, задачей которого является создание объекта Authentication на основе входящего запроса
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {
        // ...
        UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(username, password);
        // ...
        return this.getAuthenticationManager().authenticate(authRequest);
    }
// ...
```

Основная логика аутентификации описана в классе-предке `AbstractAutentificationProcessingFilter`. Именно здесь
происходит
проверка на необходимость логина и пароля:

```java
package org.springframework.security.web.authentication;

public abstract class AbstractAuthenticationProcessingFilter extends GenericFilterBean
        implements ApplicationEventPublisherAware, MessageSourceAware {

    // менеджер аутентификации

    private AuthenticationManager authenticationManager;

    // ...

    // данный объект определяет, является ли данный запрос запросом на аутентификацю
    // в классе UsernamePasswordAuthenticationFilter по-умолчанию это POST /login
    private RequestMatcher requiresAuthenticationRequestMatcher;

    // метод фильтрации
    private void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        // если метод не является запросом на авторизацию, отправляем запрос дальше
        if (!requiresAuthentication(request, response)) {
            chain.doFilter(request, response);
            return;
        }

        try {
            // аутентификация пользователя (реализация данного метода описана в UsernamePasswordAuthenticationFilter)
            Authentication authenticationResult = attemptAuthentication(request, response);

            // ...

            // успешная аутентификация
            successfulAuthentication(request, response, chain, authenticationResult);
        }
        // ...
        catch (AuthenticationException ex) {
            // Если произошла ошибка аутентификации
            unsuccessfulAuthentication(request, response, ex);
        }
    }


    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
                                            Authentication authResult) throws IOException, ServletException {
        // привязываем аутентификацию к текущему потоку
        SecurityContextHolder.getContext().setAuthentication(authResult);

        this.rememberMeServices.loginSuccess(request, response, authResult);
        // ...
    }
}
```

#### AuthenticationManager

Логику аутентификации содержат классы-имплементации `AuthenticationManager`. Сам интерфейс имеет следующую структуру:

```java
package org.springframework.security.authentication;

public interface AuthenticationManager {

    Authentication authenticate(Authentication authentication) throws AuthenticationException;

}
```

Метод `authenticate` возвращает объект `Authentication` со значением `authenticated=true` в случае успешной
аутентификации, либо выбрасывает `AuthenticationException` в противном случае. `null` вернется в случае
неопределенности.

По умолчанию, в качестве реализации `AuthenticationManager` используется `ProviderManager`. Внутри объекта этого класса
содержится цепочка экземпляров `AuthenticationProvider`-ов. Задача `ProviderManager` прогнать объект аутентификации
по цепочке провайдеров. Объекты аутентификации могут иметь разные типы (например, один из них
- `UsernamePasswordAuthenticationToken`).
Для каждого из этих типов существует своя реализация `AuthenticationProvider`. В методе `supports` объекта
`AuthenticationProvider` содержится логика проверки, подходит ли данный провайдер для работы с данным типом
аутентификации. Сам интерфейс `AuthenticationProvider` имеет структуру:

```java
package org.springframework.security.authentication;

public interface AuthenticationProvider {
    // метод для проверки соответствия типа объекта аутентификации текущему провайдеру
    boolean supports(Class<?> authentication);


    // метод аутентификации для объекта аутентификации конкретного типа
    Authentication authenticate(Authentication authentication) throws AuthenticationException;
```

Понимая назначение `AuthenticationProvider`, рассмотрим структуру `ProviderManager`:

```java
package org.springframework.security.authentication;

public class ProviderManager implements AuthenticationManager, MessageSourceAware, InitializingBean {

    // список AuthenticationProvider-ов
    private List<AuthenticationProvider> providers = Collections.emptyList();

    // родительский AuthenticatoinManager
    private AuthenticationManager parent;


    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        // класс объекта аутентификации
        Class<? extends Authentication> toTest = authentication.getClass();

        AuthenticationException lastException = null;

        // ...

        // итерируем список провайдеров
        for (AuthenticationProvider provider : getProviders()) {
            // если текущий провайдер не подошел для типа объекта входящей аутентификации - идем дальше
            if (!provider.supports(toTest)) {
                continue;
            }

            try {
                // получаем результат выполнения текущей аутентификации выбранным провайдером
                result = provider.authenticate(authentication);
                // ... 
            }
            // ...
            catch (AuthenticationException ex) {
                // если аутентификация прошла неуспешно - сохранили выбрашенное провайдером исключение
                lastException = ex;
            }
        }
        // если провайдер не смог сделать однозначный вывод по аутентификации - передаем аутентификацию parent-менеджеру аутентификаций
        if (result == null && this.parent != null) {
            // попытка выполнить аутентификацю parent-менеджером
            try {
                // запоминаем результат аутентификации
                parentResult = this.parent.authenticate(authentication);
                result = parentResult;
            }
            // ...
            catch (AuthenticationException ex) {
                // если было выброшено исключение - запоминаем его
                parentException = ex;
                lastException = ex;
            }
        }

        // если по итогу имеем положительный результат аутентифкации
        if (result != null) {
            // ...

            // возвращаем результат
            return result;
        }

        // ...

        // в случае если никто из провайдеров, а также parent-менеджер не смогли провести аутентификацию - выбрасываем последнее сохраненное исключение
        throw lastException;
    }
```

Рассмотрим механизм аутентификации в конкретном приложении `SpringSecurity`, а также разберемся с назначением
parent-менеджера аутентифкации.

В случае, когда запрос попадает в `ProviderManager` нашего приложения, мы наблюдаем следующую ситуацию:
![](images/provider.png)
Мы видим, что наш `AuthenticationProvider` в списке провайдеров содержит экземпляры `AnonymousAuthenticationProvider`
и `RememberMeAuthenticationProvider`. В качестве parent-менеджера по умолчанию задан аналогичный
экземпляр `ProviderManager`,
содержащий `DaoAuthenticationProvider` внутри списка провайдеров.

Таким образом, объект `Authentication` проходит следующий путь:

1. `AnonymousAuthenticationProvider`
2. `RememberMeAuthenticationProvider`
3. `DaoAuthenticationProvider`
   По факту, классы `AnonymousAuthenticationProvider` и `RememberMeAuthenticationProvider` не представляют для нас
   большого
   интереса, поэтому отложим их рассмотрение.

Но, какую роль играет parent-менеджер? На самом деле, в приложениях защищенные ресурсы подвергаются логической
группировке (например, все ресурсы, которые соответствуют паттерну `/api/**`, либо `/static/**` и т.д.).
Для каждой из таких групп назначается свой выделенный `AuthenticationManager`. При этом, для каждого из выделенных
менеджеров существует parent-менеджер, который является глобальным и используется, когда ни один из менеджеров не может
принять решение об аутентификации.
![](images/hierarchy.png)

В нашем случае, выделенным `AuthenticationManager`-ом является `ProviderManager`, содержащий
`AnonymousAuthenticationProvider` и `RememberMeAuthenticationProvider` и в целом для нас бесполезный.
Настоящая работа по аутентификации происходит в parent-менеджере, содержащем `DaoAuthenticationProvider`.
Именно этот менеджер настраивается в конфигурации SpringBoot-приложения с помощью `AuthenticationManagerBuilder`:

```java

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    // ...
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
    }
}
```

Сам `DaoAuthenticationProvider` содержит логику аутентификации, привязанной к `PasswordEncoder` и `UserDetailsService`.
Основная логика аутентификации с помощью `UserDetails` описана в
классе-предке `AbstractUserDetailsAuthenticationProvider`

```java
package org.springframework.security.authentication.dao;

public abstract class AbstractUserDetailsAuthenticationProvider
        implements AuthenticationProvider, InitializingBean, MessageSourceAware {

    // данный провайдер рассматривает только UsernamePasswordAuthenticationToken 
    @Override
    public boolean supports(Class<?> authentication) {
        return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        // получаем имя пользователя
        String username = determineUsername(authentication);

        // ...

        try {
            // получаем пользователя по его имени 
            UserDetails user = retrieveUser(username, (UsernamePasswordAuthenticationToken) authentication);
        }
        // в случае, если пользователь не был найден
        catch (UsernameNotFoundException ex) {
            // выбрасываем исключение
            throw new BadCredentialsException(this.messages
                    .getMessage("AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad credentials"));
        }
        // ...
    }

    // ...
    // дополнительные проверки, необходимые для аутентификации
    additionalAuthenticationChecks(user, (UsernamePasswordAuthenticationToken) authentication);
    // ...
    Object principalToReturn = user;
    // ...

    // возвращаем результат аутентификации с дополнительными механизмами гарантии наличия исходных учетных данных
		return

    createSuccessAuthentication(principalToReturn, authentication, user);
}

    protected Authentication createSuccessAuthentication(Object principal, Authentication authentication,
                                                         UserDetails user) {
        // создание данного объекта подразумевает вызов конструктора с authenticated=true у UsernamePasswordAuthenticationToken
        UsernamePasswordAuthenticationToken result = new UsernamePasswordAuthenticationToken(principal,
                authentication.getCredentials(), this.authoritiesMapper.mapAuthorities(user.getAuthorities()));
        result.setDetails(authentication.getDetails());
        return result;
    }

    // реализован в DaoAuthenticationProvider
    protected abstract UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication)
            throws AuthenticationException;

    // реализован в DaoAuthenticationProvider
    protected abstract void additionalAuthenticationChecks(UserDetails userDetails,
                                                           UsernamePasswordAuthenticationToken authentication) throws AuthenticationException;

// ...
```

В классе `DaoAuthenticationProvider` нас интересуют методы `retrieveUser()` и `additionalAuthenticationChecks()`

```java
package org.springframework.security.authentication.dao;

public class DaoAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

    // ...

    private PasswordEncoder passwordEncoder;

    private UserDetailsService userDetailsService;

    @Override
    protected final UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication)
            throws AuthenticationException {

        // ...
        try {
            // получение пользователя из сервиса
            UserDetails loadedUser = this.getUserDetailsService().loadUserByUsername(username);
            // если пользователь не получен, выбрасываем исключение
            if (loadedUser == null) {
                throw new InternalAuthenticationServiceException(
                        "UserDetailsService returned null, which is an interface contract violation");
            }
            // возвращаем загруженного пользователя
            return loadedUser;
        }
        // в случае возникновения исключения - оборачиваем/пробрасываем их 
        catch (UsernameNotFoundException ex) {
            // ...
            throw ex;
        } catch (InternalAuthenticationServiceException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new InternalAuthenticationServiceException(ex.getMessage(), ex);
        }
    }


    @Override
    @SuppressWarnings("deprecation")
    protected void additionalAuthenticationChecks(UserDetails userDetails,
                                                  UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        // если отсутствуют данные для аутентификации
        if (authentication.getCredentials() == null) {
            // выбрасываем исключение
            throw new BadCredentialsException(this.messages
                    .getMessage("AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad credentials"));
        }
        // получаем текущий пароль пользователя
        // выполняем проверку пароля с помощью passwordEncoder
        String presentedPassword = authentication.getCredentials().toString();
        if (!this.passwordEncoder.matches(presentedPassword, userDetails.getPassword())) {
            throw new BadCredentialsException(this.messages
                    .getMessage("AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad credentials"));
        }
    }

```

Таким образом, результатом работы цепочки: `UsernamePasswordAuthenticationFilter` -> `AuthenticationManager` ->
`DaoAuthenticationProvider` является объект `Authentication`, содержащий значение `authenticated=true`, далее этот
объект помещается в `SecurityContext`, с помощью `SecurityContextHolder` в `AbstractAuthenticationProcessingFilter`.

### Авторизация

После того, как для запроса был создан объект `Authentication` с указанным параметром аутентификации, необходимо
проверить, имеет ли пользователь доступ к запрашиваемому URL.

Пусть имеем следующую настройку конфигурации Spring Security:

```java
.antMatchers("/users").hasAuthority("ADMIN");
```

В таком случае для аутентифицированного пользователя запрос после прохождения цепочки фильтров попадает в последний -
`FilterSecurityInterceptor`, задачей которого является авторизация пользователя по URL.

```java
public class FilterSecurityInterceptor extends AbstractSecurityInterceptor implements Filter {

    // ...

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        invoke(new FilterInvocation(request, response, chain));
    }

    public void invoke(FilterInvocation filterInvocation) throws IOException, ServletException {
        // ...

        // авторизация запроса
        InterceptorStatusToken token = super.beforeInvocation(filterInvocation);

        try {
            // направление запроса далее по цепочке
            filterInvocation.getChain().doFilter(filterInvocation.getRequest(), filterInvocation.getResponse());
        }
        // ...
    }
```

Класс `FilterSecurityInterceptor` является потомком класса `AbstractSecurityInterceptor`, именно в нем вызывается
основная логика по работе с авторизацией.

```java
package org.springframework.security.access.intercept;

public abstract class AbstractSecurityInterceptor
        implements InitializingBean, ApplicationEventPublisherAware, MessageSourceAware {

    // менеджер авторизации
    private AccessDecisionManager accessDecisionManager;

    // object содержит информацию о самом запросе, в данном случае - GET /users
    protected InterceptorStatusToken beforeInvocation(Object object) {
        // ...

        // получение определенных атрибутов (правил), позволяющих определить владельца ресурса (т.е. выполнить авторизацию)
        // в данном случае - строка hasAuthority('ADMIN')
        Collection<ConfigAttribute> attributes = this.obtainSecurityMetadataSource().getAttributes(object);
        // получение текущего объекта аутентификации из SecuryContextHolder-а
        Authentication authenticated = authenticateIfRequired();
        // выполнение авторизации
        attemptAuthorization(object, attributes, authenticated);
        // ...
    }


    private void attemptAuthorization(Object object, Collection<ConfigAttribute> attributes,
                                      Authentication authenticated) {
        try {
            // выполнение авторизации с помощью AcessDecisionManager
            this.accessDecisionManager.decide(authenticated, object, attributes);
        } catch (AccessDeniedException ex) {
            throw ex;
        }
    }
}
```

В свою очередь, интерфейс `AcessDecisitionManager` содержит главный метод авторизации:

```java
package org.springframework.security.access;

public interface AccessDecisionManager {
    // метод принимает решение по предоставлению доступа
    void decide(Authentication authentication, Object object, Collection<ConfigAttribute> configAttributes)
            throws AccessDeniedException, InsufficientAuthenticationException;
}
```

`AccessDecisionManager` имплементируется несколькими классами, наибольий интерес представляет класс
`AffirmativeBased`. Данный класс является потомком класса `AbstractAccessDecisionManager`:

```java
package org.springframework.security.access.vote;

public abstract class AbstractAccessDecisionManager
        implements AccessDecisionManager, InitializingBean, MessageSourceAware {

    private List<AccessDecisionVoter<?>> decisionVoters;

    // ...
}
```

Каждый из `AccessDecisionVoter` имеет
метод, `int vote(Authentication authentication, S object, Collection<ConfigAttribute> attributes);`
Данный метод определяет, имеет ли пользователь с аутентификацией `authentication` доступ к объекту `object`,
учитывая определенный набор правил доступа `attributes`.

Сам `AffirmativeBased` имеет следующую структуру:

```java
package org.springframework.security.access.vote;

public class AffirmativeBased extends AbstractAccessDecisionManager {

    // ... 
    @Override
    @SuppressWarnings({"rawtypes", "unchecked"})
    public void decide(Authentication authentication, Object object, Collection<ConfigAttribute> configAttributes)
            throws AccessDeniedException {
        int deny = 0;
        // итерируем список voter-ов
        for (AccessDecisionVoter voter : getDecisionVoters()) {
            // получаем результат, имеет ли пользователь authentication доступ к object с учетом правил configAttributes
            int result = voter.vote(authentication, object, configAttributes);
            // если хотя бы один из voter-ов дал доступ, авторизация считается пройденной
            switch (result) {
                case AccessDecisionVoter.ACCESS_GRANTED:
                    return;
                case AccessDecisionVoter.ACCESS_DENIED:
                    deny++;
                    break;
                default:
                    break;
            }
        }
        // если доступ никто не разрешил, при этом имеются запреты на этот запрос, то выбрасываем сключение
        if (deny > 0) {
            throw new AccessDeniedException(
                    this.messages.getMessage("AbstractAccessDecisionManager.accessDenied", "Access is denied"));
        }

        // ...
    }
```

В нашем случае наблюдаем следующую картину

![](images/voters.png)

Следовательно, внутри `AttempBased` с помощью единственного voter-а `WebExpressionVoter` происходит решение,
стоит ли предоставить доступ к объекту `FilterInvocation` `[GET /users]` с учетом
атрибута-правила `hasAuthority('ADMIN')`.

### Spring Security: Part II

- `BCryptPasswordEncoder` – применяет надежное шифрование bcrypt;
- `NoOpPasswordEncoder` – не применяет шифрования;
- `Pbkdf2PasswordEncoder` – применяет шифрование PBKDF2;
- `SCryptPasswordEncoder` – применяет шифрование Scrypt;
- `StandardPasswordEncoder` – применяет шифрование SHA-256.

Сравнение выполняет метод `matches()` объекта `PasswordEncoder`.

Чтобы настроить хранилище учетных записей пользователей для их
аутентификации, нам понадобится bean-компонент UserDetailsService.
Интерфейс `UserDetailsService` относительно прост и имеет все-
го один метод, который нужно реализовать. Вот как выглядит `UserDetailsService`:

```java
public interface UserDetailsService {
    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;
}
```

Метод `loadUserByUsername()` принимает имя пользователя и отыс-  
кивает соответствующий объект `UserDetails`. Если учетная запись
с таким именем пользователя не будет найдена, то метод сгенерирует
исключение `UsernameNotFoundException`.
Как оказывается, в Spring Security есть несколько готовых реализа-  
ций `UserDetailsService`, в том числе:

- хранилище учетных записей в памяти;
- хранилище учетных записей `JDBC`;
- хранилище учетных записей `LDAP`.
  Но вы, если понадобится, можете создать свою реализацию, отве-
  чающую вашим конкретным потребностям.

#### Защита веб-запросов

Метод `filterChain()` принимает объект `HttpSecurity`, который дей-  
ствует как построитель, который можно использовать для настрой-  
ки работы системы безопасности на веб-уровне. После настройки
конфигурации безопасности с помощью объекта `HttpSecurity` вызов
`build()` создаст компонент `SecurityFilterChain`.  
Помимо всего прочего, с помощью HttpSecurity можно:

- потребовать выполнения определенных условий безопасности
  перед обслуживанием запроса;
- отправить пользователю свою страницу входа;
- предоставить пользователям возможности выйти из приложения;
- настроить защиту от подделки межсайтовых запросов.

```java
    @Bean
public SecurityFilterChain filterChain(HttpSecurity http)throws Exception{
        return http
        .authorizeRequests()
        .antMatchers("/design","/orders").hasRole("USER")
        .antMatchers("/","/**").permitAll()
        .and()
        .build();
        }
```

- запросы с путями `/design` и `/orders` должны обрабатываться, толь-  
  ко если они отправлены пользователями с подтвержденными
  привилегиями `ROLE_USER`. Не включайте префикс `ROLE_` в назва-  
  ния ролей, передаваемых в вызов `hasRole()`, – он подразумева-  
  ется по умолчанию;
- все остальные запросы должны обрабатываться безоговорочно.

![](images/filter_chain.png)
![](images/spel.png)

Как видите, большинство расширений в табл. 5.2 соответствуют
аналогичным методам в табл. 5.1. На самом деле, используя метод `access()`
с выражениями `hasRole` и `PermitAll`, можно переписать конфи-  
гурацию `SecurityFilterChain`, как показано ниже.

```java
@Bean
public SecurityFilterChain filterChain(HttpSecurity http)throws Exception{
        return http
        .authorizeRequests()
        .antMatchers("/design","/orders").access("hasRole(‘USER’)")
        .antMatchers("/","/**").access("permitAll()")
        .and()
        .build();
        }
```

На первый взгляд ничего особенного. В конце концов, эти выра-  
жения лишь отражают то, что мы делали выше с использованием ме-  
тодов. Но выражения могут быть гораздо более гибкими. Например,  
представьте, что (по какой-то безумной причине) нам понадобилось  
разрешить создавать новые тако только пользователям с полномочия-  
ми `ROLE_USER` и только по вторникам. В таком случае мы могли бы пе-  
реписать выражение, как показано в следующей ниже версии метода  
bean-компонента `SecurityFilterChain`:

```java
    @Bean
public SecurityFilterChain filterChain(HttpSecurity http)throws Exception{
        return http
        .authorizeRequests()
        .antMatchers("/design","/orders")
        .access("hasRole('USER')")
        .antMatchers("/","/**").permitAll()
        .and()
        .build();
        }
```

Метод `and()` означает, что мы закончили настройку авторизации и готовы
применить некоторую дополнительную настройку HTTP. Мы будем
использовать `and()` всякий раз, начиная новый раздел конфигурации.

#### @PostAuthorize

Аннотация `@PostAuthorize` действует
почти так же, как `@PreAuthorize`, но ее выражение вычисляется только
после вызова целевого метода. Это позволяет использовать в выраже-
нии значение, возвращаемое методом, и учитывать его при принятии
решения.
Например, предположим, что у нас есть метод, извлекающий заказ
по его идентификатору. Если вы решите ограничить возможность его
использования только администраторами или пользователем, соз-
давшим заказ, то можете использовать `@PostAuthorize`, как показано
ниже:

```java
@PostAuthorize("hasRole(‘ADMIN’) || " +
        "returnObject.user.username == authentication.name")
public TacoOrder getOrder(long id){
        ...
        }
```

В этом случае `returnObject` – это `TacoOrder`, возвращаемый мето-  
дом. Если его свойство user содержит то же имя пользователя, что
хранится в свойстве `authentication.name`, то дальнейшее использова-  
ние возвращаемого значения будет разрешено. Однако, чтобы выпол-  
нить сравнение, нужно вызвать метод и получить объект `TacoOrder`.

У нас есть несколько способов определить пользователя. Вот неко-
торые наиболее распространенные:

- внедрить объект `java.security.Principal` в метод контроллера;
- внедрить объект `org.springframework.security.core.Authentication` в метод контроллера;
- использовать `org.springframework.security.core.context.SecurityContextHolder`, чтобы получить контекст безопасности;
- внедрить параметр метода с аннотацией `@AuthenticationPrincipal`. (Аннотация `@AuthenticationPrincipal` реализована в
  пакете
  `org.springframework.security.core.annotation`.)

### Cassandra

`Cassandra` – это распределенная, высокопроизводительная, постоян-
но доступная, в конечном итоге согласованная база данных NoSQL
с секционированным колоночным хранилищем.

`Cassandra` охватывает кластер узлов, которые все вместе действуют
как единая система баз данных. Если у вас еще нет кластера
`Cassandra` для работы, то для нужд разработки вы можете запустить кластер
с одним узлом, используя `Docker`:

```text
$ docker network create cassandra-net
$ docker run --name my-cassandra \
             --network cassandra-net \
             -p 9042:9042 \
             -d cassandra:latest
```

Эта команда запустит кластер с одним узлом и откроет порт узла
(9042) на хост-компьютере, через который наше приложение сможет
получить к нему доступ.
Но нам нужно добавить некоторые настройки, в частности опреде-  
лить имя пространства ключей, в котором будут работать репозито-  
рии. Однако прежде нужно создать такое пространство ключей.

Пространство ключей в Cassandra обеспечива-  
ет группировку таблиц. Примерно так же группируются табли-  
цы, представления и ограничения в реляционных базах данных.

`$ docker run -it --network cassandra-net --rm cassandra cqlsh my-cassandra` - Cassandra query language

```text
cqlsh> create keyspace tacocloud
    ... with replication={‘class’:’SimpleStrategy’, ‘replication_factor’:1}
    ... and durable_writes=true;
```

Эта команда создаст пространство ключей с именем tacocloud
с простой репликацией и надежной записью. Установив коэффици-  
ент репликации равным 1, мы просим Cassandra сохранять по одной
копии каждой записи. Стратегия репликации определяет порядок
создания копий записей. Стратегия SimpleStrategy хорошо подходит
для случаев, когда все узлы кластера с базой данных Cassandra на-  
ходятся в одном вычислительном центре (и для демонстрационного
кода, конечно). Если узлы кластера разбросаны по нескольким вы-  
числительным центрам, то можно использовать стратегию Network-  
TopologyStrategy. За более подробной информацией об особенностях
разных стратегий репликации и альтернативных способах создания
пространств ключей обращайтесь к документации с описанием Cas-  
sandra.

- таблицы Cassandra могут иметь любое количество столбцов, но
  от записей не требуется использовать их все;
- базы данных Cassandra разделены на несколько сегментов, или
  разделов. Любая запись в данной таблице может храниться в од-  
  ном или нескольких разделах, но весьма маловероятно, что каж-  
  дый из разделов будет хранить все записи;
- таблицы в Cassandra имеют два типа ключей: ключи сегменти-  
  рования и ключи кластеризации. Хеш-операции выполняются
  с ключом сегментирования для каждой записи, чтобы опреде-  
  лить, какой раздел(ы) будет хранить эту запись. Ключи класте-  
  ризации определяют порядок, в каком записи будут храниться
  в разделе (этот порядок может не совпадать с порядком, в каком
  записи могут возвращаться в результатах запроса). Более пол-  
  ную информацию о моделировании данных в Cassandra, разде-  
  лах, кластерах и соответствующих ключах ищите в документа-  
  ции (http://mng.bz/yJ6E);
- Cassandra оптимизирована для операций чтения. По этой при-  
  чине обычно желательно, чтобы таблицы были сильно денор-  
  мализованы, а данные повторялись в нескольких таблицах. (На-  
  пример, информация о клиентах может храниться в таблице
  клиентов и повторяться в таблице заказов, размещенных кли-  
  ентами.)

#### Аннотации Cassandra

- `@PrimaryKeyColumn` с атрибутом `type=rimaryKeyType.PARTITIONED`. Он
  указывает, что свойство `id` играет роль ключа раздела, используемого
  для выбора раздела или разделов, где Cassandra будет хранить записи
  с экземплярами Taco.
- Далее следует определение свойства `createdAt`, которое служит
  вторым столбцом первичного ключа. Но в этом случае атрибут `type`
  в аннотации `@PrimaryKeyColumn` имеет значение `PrimaryKeyType.CLUSTERED`,
  вследствие чего свойство `createdAt` становится ключом класте-  
  ризации. Как упоминалось выше, ключи кластеризации используют-  
  ся для определения порядка хранения записей в разделе – в данном
  случае в порядке убывания, поэтому в пределах данного раздела бо-  
  лее новые записи будут помещаться в начало таблицы `tacos`.
- Наконец, свойство `ingredients` теперь объявлено как список объ-  
  ектов типа `IngredientUDT`, а не `Ingredient`. Как вы, наверное, помни-  
  те, таблицы в `Cassandra` сильно денормализованы и могут содержать
  повторяющиеся данные из других таблиц. Таблица `ingredient` будет
  хранить записи для всех доступных ингредиентов, но ингредиенты из
  списка ingredients в данном экземпляре Taco будут продублированы
  в столбце `ingredients`. Вместо ссылок на записи в таблице `ingredients`
  свойство `ingredients` будет хранить полные данные для каждого вы-  
  бранного ингредиента. Но зачем понадобилось вводить новый класс `IngredientUDT`? По-  
  чему нельзя просто использовать класс `Ingredient`? Дело в том, что
  столбцы, хранящие коллекции данных, такие как столбец `ingredients`,
  должны быть коллекциями встроенных типов (целых чисел, строк
  и т. д.) или типов, определяемых пользователем.

### Spring Mongo

На этапе тестирования и разработки проще и удобнее исполь-  
зовать встраиваемую базу данных MongoDB. Для этого подключите
`Flapdoodle` – встраиваемую версию MongoDB, – добавив следующую
зависимость в спецификацию сборки:

```xml

<dependency>
    <groupId>de.flapdoodle.embed</groupId>
    <artifactId>de.flapdoodle.embed.mongo</artifactId>
    <!-- <scope>test</scope> -->
</dependency>
```

Встраиваемая база данных `Flapdoodle` обеспечивает такое же удоб-  
ство работы с базой данных Mongo в памяти, как и H2 в отношении
реляционных баз данных. То есть вам не придется запускать отдель-  
ный сервер баз данных, но все данные исчезнут после остановки при-  
ложения.

#### Отображение типов данных предметной области в документы

- `@Id` – объявляет свойство уникальным идентификатором доку-
  мента (из Spring Data Commons);
- `@Document` – объявляет тип данных документом, который может
  храниться в MongoDB;
- `@Field` – определяет имя поля (и дополнительно порядок сорти-
  ровки) для сохранения свойства в хранимом документе;
- `@Transient` – определяет свойство, которое не будет сохраняться
  в базе данных.

Из этих аннотаций только `@Id` и `@Document` абсолютно необходимы.
Если свойства не снабдить аннотацией `@Field` или `@Transient`, то они
будут сохраняться в полях с именами, повторяющими имена свойств.

Подход к хранимым документам в MongoDB хорошо укладывается
в идею предметно-ориентированного проектирования с сохранени-  
ем корневых агрегатов. Документы в MongoDB, как правило, опреде-  
ляются как корни агрегатов, а составляющие агрегата – как вложен-  
ные документы.
Для Taco Cloud это означает следующее: так как объекты Taco всег-  
да будут сохраняться только как составляющие агрегата TacoOrder,
класс Taco не нужно снабжать аннотациями `@Document` и `@Id`. Он может
оставаться свободным от любых аннотаций механизма хранения.

Однако класс TacoOrder – это корень агрегата и должен снабжаться
аннотацией `@Document`, а также иметь свойство с аннотацией `@Id`.

Однако обратите внимание, что тип свойства `id` изменился на
`String` (в отличие от `Long` в версии `JPA` или `UUID` в версии `Cassandra`).
Как я уже говорил, аннотацию `@Id` можно применить к любому типу
`Serializable`. Но, выбрав тип `String` для свойства-идентификатора,
мы получаем дополнительную выгоду – база данных Mongo автома-
тически будет присваивать ему значение при сохранении (при усло-
вии что оно имеет значение `null`). Выбирая тип `String`, мы получаем
значение идентификатора от базы данных, и отпадает необходимость
заботиться о выборе значения для этого свойства вручную.
Конечно, есть несколько сложных и необычных случаев, однако
чаще для отображения предметных типов для хранения в MongoDB
достаточно аннотаций `@Document` и `@Id`, иногда `@Field` или `@Transient`.
И именно к таким частым случаям относится Taco Cloud.
Теперь осталось только определить интерфейсы репозитория.

### Создание служб REST

```java

@RestController
@RequestMapping(path = "/api/tacos", produces = "application/json")
// -> produces будет предполагать, что запрос клиента 
// включает Header: Accept: application/json
@CrossOrigin(origins = {"http://localhost:3000", "https://tacocloud.com"}) // -> настройка CORS
public class TacoController {

    private TacoRepository tacoRepo;

    public TacoController(TacoRepository tacoRepo) {
        this.tacoRepo = tacoRepo;
    }

    @GetMapping(params = "recent") // -> срабатывает если есть параметр запроса /api/tacos?recent
    public Iterable<Taco> recentTacos() {
        PageRequest page = PageRequest.of(0, 12, Sort.by("createdAt").descending());
        return tacoRepo.findAll(page).getContent();
    }


    @PostMapping(consumes = "application/json") // -> определяет формат входных данных в запросе
    @ResponseStatus(HttpStatus.CREATED)
    public Taco postTaco(@RequestBody Taco taco) {
        return tacoRepo.save(taco);
    }


    // PUT помещает данные, полностью заменяет все имеющиеся. Если какое-то свойство отсутствует, то оно будет затерто
    // нулевым значением
    @PutMapping(path = "/{orderId}", consumes = "application/json")
    public TacoOrder putOrder(
            @PathVariable("orderId") Long orderId,
            @RequestBody TacoOrder order) {
        order.setId(orderId);
        return repo.save(order);
    }


    @PatchMapping(path = "/{orderId}", consumes = "application/json")
    public TacoOrder patchOrder(@PathVariable("orderId") Long orderId,
                                @RequestBody TacoOrder patch) {
        TacoOrder order = repo.findById(orderId).get();
        if (patch.getDeliveryName() != null) {
            order.setDeliveryName(patch.getDeliveryName());
        }
        if (patch.getDeliveryStreet() != null) {
            order.setDeliveryStreet(patch.getDeliveryStreet());
        }
        // ...
        if (patch.getCcCVV() != null) {
            order.setCcCVV(patch.getCcCVV());
        }
        return repo.save(order);
    }
}
```

#### RestTemplate

![](images/rest_template.png)

Сложный способ:

```java
public Ingredient getIngredientById(String ingredientId){
        Map<String, String> urlVariables=new HashMap<>();
        urlVariables.put("id",ingredientId);
        URI url=UriComponentsBuilder
        .fromHttpUrl("http://localhost:8080/ingredients/{id}")
        .build(urlVariables);

        return rest.getForObject(url,Ingredient.class);
        }
```

### Безопасноть REST

![](images/oauth.png)

Далее мы будем использовать процесс предоставления кода авто-
ризации, чтобы получить токен доступа JSON Web Token (JWT). Для
этого нам потребуется создать несколько приложений, взаимодей-
ствующих друг с другом:

- Сервер авторизации – задача сервера авторизации заключается
  в том, чтобы получить от пользователя разрешение, позволя-
  ющее клиентскому приложению выступать от его имени. Если
  пользователь даст разрешение, то сервер авторизации предоста-
  вит токен доступа клиентскому приложению, который оно смо-
  жет использовать для доступа к API;
- Сервер ресурсов – это просто другое название API, защищенно-
  го с помощью OAuth 2. Сервер ресурсов является частью самого
  API, однако в дальнейшем обсуждении мы будем разделять эти
  два понятия. Сервер ресурсов ограничивает доступ к своим ре-
  сурсам, если запрос не предоставляет доступ к действительному
  токену с необходимым набором разрешений. Для нас сервером
  ресурсов будет служить Taco Cloud API, который мы начали соз-
  давать в главе 7, мы лишь добавим в него некоторые настройки
  безопасности;
- Клиентское приложение – это приложение, использующее API, для
  доступа к которому требуется разрешение. Мы напишем простое
  приложение администратора для Taco Cloud, которое позволит
  добавлять новые ингредиенты;
- Пользователь – человек, использующий клиентское приложение
  и дающий ему разрешение на доступ к API сервера ресурсов от
  своего имени.

### Authorization Server

Метод authenticationServerSecurityFilterChain() определяет SecurityFilterChain,
который настраивает некоторое поведение по
умолчанию для сервера авторизации OAuth 2 и страницу входа по
умолчанию. Аннотация @Order присваивает Ordered.HIGHEST_PRECEDENCE,
гарантируя, что если по какой-то причине будут объявлены
другие bean-компоненты этого же типа, то данный компонент будет
иметь приоритет над другими.

Единственный нестандартный компонент, который не предостав-  
ляется `OAuth2AuthorizationServerConfiguration`, – это репозиторий
клиентов. Этот репозиторий подобен службе с информацией о пользо-  
вателях, только вместо сведений о пользователях он хранит сведения
о клиентах, которые могут запрашивать авторизацию от имени поль-  
зователей. Он определяется интерфейсом RegisteredClientRepository:

```java
public interface RegisteredClientRepository {
    @Nullable
    RegisteredClient findById(String id);

    @Nullable
    RegisteredClient findByClientId(String clientId);
}
```

Для промышленного окружения можно написать свою реализацию
RegisteredClientRepository, чтобы получать сведения о клиенте из  
базы данных или из другого источника. Но по умолчанию Spring Au-  
thorization Server использует реализацию в памяти, которая идеально
подходит для демонстрации и тестирования. Вы можете реализовать
RegisteredClientRepository так, как сочтете нужным, но в этой кни-  
ге мы будем использовать реализацию в памяти и зарегистрируем
одного клиента на сервере авторизации.

### JWT

* Некоторая строка, которую может проверить сервер и сделать выводы, только на основе этой строки
* Данная строка подписывыается сервером, поэтому он может ей доверять
* Главное преимущество - не нужно идти в базу с каждым запросом (как это было с `TokenRepository`)

#### Структура токена

* Заголовок (Header), содержит служебную информацию, в частности алгоритм хеширования.

```json
{
  "alg": "HS256",
  "typ": "JWT"
}
```

* Полезная нагрузка (Payload) - содержит важные данные, которые мы хотим направлять от клиента к серверу (кроме пароля)

```json
{
  "sup": "3",
  "email": "kalim@mail.ru",
  "state": "CONFIRMED",
  "role": "ADMIN"
}
```

* HMACSHA256 - алгоритм хеширования, поволяет захешировать какую-либо строку с использованием какого-либо ключа.
* Это значит, что мы можем проверить любую строку на предмет того, что из нее можно получить тот же самый хеш с тем же
  ключом.

* Сигнатура для верификации (Verify Signature) - она позволяет серверу доверять данным, которые есть в payload.

```text
base64(header) + "." + base64(payload) = 
fweljfisdfkjw;elijfisd.fiwejfojwoidfjskdjfljeifweiofjijklsdjflewjfisdjkfljsldjf

verify_signature -> HMACSHA256(base64(header) + "." + base64(payload), secretKey) = werrheufhuhdfiuehf89shfuh8w9f49h
```

verify_signature формируется на основании paylaod'а и некого секретного ключа, который известен только серваку.

### JMS (Apache Artemis)

JmsTemplate – центральный элемент интеграции Spring с JMS. По-  
добно другим компонентам Spring, ориентированным на шаблоны,
JmsTemplate избавляет от необходимости писать большой объем шаб
лонного кода. Без JmsTemplate вам придется написать код, открываю-  
щий соединение с брокером сообщений и создающий сеанс, а также
дополнительный код для обработки любых исключений, которые мо-  
гут возникнуть в процессе отправки сообщения. JmsTemplate позволя-  
ет сосредоточиться на том, что вы действительно хотите сделать: на
отправке сообщений.

#### Отправка

JmsTemplate имеет несколько методов отправки сообщений, в том
числе:

```java
// Отправка неструктурированных сообщений
        void send(MessageCreator messageCreator)throws JmsException;
                void send(Destination destination,MessageCreator messageCreator)throws Exception;
                void send(String destinationName,MessageCreator messageCreator)throws JmsException;
// Отправка сообщений в форме объектов
                void convertAndSend(Object message)throws JmsException;
                void convertAndSend(Destination destination,Object message)throws JmsException;
                void convertAndSend(String destinationName,Object message)throws JmsException;
// Отправка сообщений в форме объектов с постобработкой
                void convertAndSend(Object message,MessagePostProcessor postProcessor)throws JmsException;
                void convertAndSend(Destination destination,Object message,MessagePostProcessor postProcessor)throws JmsException;
                void convertAndSend(String destinationName,Object message,MessagePostProcessor postProcessor)throws JmsException;
```

```java

@Service
@RequiredArgsConstructor
public class JmsUserMessagingService {
    private final JmsTemplate jmsTemplate;
    private final Destination userQueue;

    public void sendUser(User user) {
        jmsTemplate.send(
                userQueue, // или можно просто "outlaw.users.queue"
                session -> session.createObjectMessage(user));
    }

    public void convertAndSendUser(User user) { // то же, что и send(), но автоматически конвертирует в Message
        jmsTemplate.convertAndSend("outlaw.users.queue", user);
    }
}
```

```java
public interface MessageConverter {
    Message toMessage(Object object, Session session) throws JMSException, MessageConversionException;

    Object fromMessage(Message message)
}
```

![](images/jms_convertors.png)

#### Получение

Когда дело доходит до получения сообщений, появляется возмож-  
ность выбора между моделями активного (`pull`), когда клиентский код
запрашивает получение сообщения и ждет его поступления, и пассив-  
ного (`push`) извлечения сообщений, когда клиент автоматически полу-  
чает сообщения по мере их появления.

Оба варианта подходят для разных случаев использования. Приня-  
то считать, что модель пассивного получения (`push`) – лучший выбор,
потому что не блокирует поток выполнения. Но иногда приемник
может не справляться с нагрузкой, если сообщения поступают слиш-  
ком часто. Модель активного извлечения (`pull`) позволяет получателю
объявить, что он готов обработать следующее сообщение.

```java
Message receive()throws JmsException;
        Message receive(Destination destination)throws JmsException;
        Message receive(String destinationName)throws JmsException;
        Object receiveAndConvert()throws JmsException;
        Object receiveAndConvert(Destination destination)throws JmsException;
        Object receiveAndConvert(String destinationName)throws JmsException;
```

### RabbitMQ и AMQP

RabbitMQ – пожалуй, самая известная реализация AMQP, предлагаю-  
щая более мощную стратегию маршрутизации сообщений, чем JMS.
В JMS сообщения адресуются именем места назначения, откуда полу-    
чатель извлекает их, сообщения AMQP адресуются именем обменни-    
ка и ключом маршрутизации, которые отделены от очереди, просмат
риваемой получателем. Эта связь между обменником и очередями
показана на рис. 9.2.

![](images/rabbit.png)

Сообщение, отправленное брокеру RabbitMQ, сохраняется в обмен-  
нике (exchange), которому оно было адресовано. Обменник отвечает
за маршрутизацию сообщений в одну или несколько очередей, в за-  
висимости от типа обменника, соединений между обменником и оче-  
редями и значения ключа маршрутизации сообщения.
Существует несколько видов обменников, в том числе:
по умолчанию – специальный обменник, автоматически созда
ваемый брокером. Он направляет сообщения в очереди, имена
которых совпадают с ключом маршрутизации сообщения. Все
очереди автоматически соединяются с обменником по умолча-  
нию;

- `прямой` – направляет сообщения в очередь, ключ соединения ко-
  торой совпадает с ключом маршрутизации сообщения;
    - `тема` – направляет сообщения в одну или несколько очередей
      с ключом соединения (который может содержать подстано-
      вочные знаки), соответствующим ключу маршрутизации сооб
      щения;
- `разветвление` – направляет сообщения во все очереди, соединен-
  ные с обменником, без учета ключей соединения или маршру-
  тизации;
- `заголовки` – действует подобно темам, но для маршрутизации ис-
  пользует значения заголовков сообщений, а не ключи маршру-
  тизации;
- `недоставленные сообщения` – собирает все сообщения, которые
  невозможно доставить (т. е. они не соответствуют какому-то
  определенному соединению с очередью).

Простейшие формы обменников – обменник по умолчанию и об-  
менник разветвления. Они примерно соответствуют очереди и теме
в JMS. Другие обменники позволяют определять более гибкие схемы
маршрутизации.
Самое важное, что нужно понять, – сообщения отправляются в об-  
менники с ключами маршрутизации, а извлекаются из очередей. Как
они попадают из обменника в очередь, зависит от настроек соедине-  
ния, лучше всего соответствующих вашим потребностям.
Выбор типа обменника и настроек соединений между обменника-  
ми и очередями мало влияет на порядок отправки и приема сообще-  
ний в приложениях Spring. Поэтому мы сосредоточимся на том, как
писать код, который отправляет и получает сообщения с помощью
Rabbit.

#### Отправка сообщений

```java
// Отправка неструктурированных сообщений
void send(Message message)throws AmqpException;
        void send(String routingKey,Message message)throws AmqpException;
        void send(String exchange,String routingKey,Message message)
        throws AmqpException;
// Отправка сообщений в форме объектов
        void convertAndSend(Object message)throws AmqpException;
        void convertAndSend(String routingKey,Object message)
        throws AmqpException;
        void convertAndSend(String exchange,String routingKey,
        Object message)throws AmqpException;
// Отправка сообщений в форме объектов с постобработкой
        void convertAndSend(Object message,MessagePostProcessor mPP)
        throws AmqpException;
        void convertAndSend(String routingKey,Object message,
        MessagePostProcessor messagePostProcessor)
        throws AmqpException;
        void convertAndSend(String exchange,String routingKey,
        Object message,
        MessagePostProcessor messagePostProcessor)
        throws AmqpException;
```

#### Настройка конвертеров

Jackson2JsonMessageConverter – преобразует объекты в формат
JSON и обратно с помощью процессора Jackson 2 JSON;

- MarshallingMessageConverter – выполняет преобразования с по
  мощью Marshaller и Unmarshaller в Spring;
- SerializerMessageConverter – преобразует строки и простые объ-  
  екты любого типа, используя абстракции Serializer и Deserializer;
- SimpleMessageConverter – преобразует строки, массивы байтов
  и сериализуемые типы;
- ContentTypeDelegatingMessageConverter – делегирует выполне-  
  ние преобразований другому MessageConverter, опираясь на за-  
  головок contentType;
- MessagingMessageConverter – делегирует преобразование сооб-  
  щений базовому MessageConverter и заголовков – AmqpHeaderConverter.

#### Получение сообщений

Как мы увидели, отправка сообщений с помощью RabbitTemplate мало
отличается от отправки сообщений с помощью JmsTemplate. И как
оказывается, получение сообщений из очереди RabbitMQ не сильно
отличается от получения сообщений из JMS.
Так же как при использовании JMS, у нас есть два варианта:

- Активное извлечение сообщений из очереди с помощью RabbitTemplate;
- Пассивный прием сообщений в методе с аннотацией @RabbitListener.

Начнем с активного извлечения сообщений вызовом метода Rab-
bitTemplate.receive().

```java
// Извлечение сообщений
Message receive()throws AmqpException;
        Message receive(String queueName)throws AmqpException;
        Message receive(long timeoutMillis)throws AmqpException;
        Message receive(String queueName,long timeoutMillis)throws AmqpException;
// Извлечение объектов из сообщений
        Object receiveAndConvert()throws AmqpException;
        Object receiveAndConvert(String queueName)throws AmqpException;
        Object receiveAndConvert(long timeoutMillis)throws AmqpException;
        Object receiveAndConvert(String queueName,long timeoutMillis)
        throws AmqpException;
// Извлечение объектов из сообщений с соблюдением безопасности типов
<T> T receiveAndConvert(ParameterizedTypeReference<T> type)
        throws AmqpException;
<T> T receiveAndConvert(
        String queueName,ParameterizedTypeReference<T> type)
        throws AmqpException;
<T> T receiveAndConvert(
        long timeoutMillis,ParameterizedTypeReference<T> type)
        throws AmqpException;
<T> T receiveAndConvert(String queueName,long timeoutMillis,
        ParameterizedTypeReference<T> type)
        throws AmqpException;
```

Эти методы являются зеркальным отражением методов send()
и convertAndSend(), описанных выше. Методы receive() возвращают
сообщение Message без всякой обработки, а методы receiveAndCon-  
vert() используют настроенный конвертер для преобразования со-  
общений в типы данных предметной области.
Но в сигнатурах методов есть несколько заметных отличий. Во-  
первых, ни один из этих методов не имеет параметра с именем об-  
менника или ключом маршрутизации. Это связано с тем, что имя
обменника и ключ маршрутизации необходимы, чтобы доставить со-  
общение в очередь, но для извлечения сообщения из очереди эта ин-  
формация не требуется. Приложениям-потребителям не нужно знать
имя обменника или ключ маршрутизации. Очередь – это единствен-  
ное, о чем должны знать приложения-потребители.
Отметьте также, что многие методы принимают параметр Long,
определяющий время ожидания сообщений в миллисекундах. По
умолчанию время ожидания составляет 0 мс, т. е. вызов receive()
сразу же вернет управление, даже если в очереди нет сообщений.
Это заметное отличие от поведения методов receive() в JmsTemplate.
Задавая значение тайм-аута, можно блокировать методы receive()
и receiveAndConvert() до тех пор, пока не поступит сообщение или
пока не истечет тайм-аут. Но, даже задавая ненулевой тайм-аут, вы
должны быть готовы к тому, что ваш код вернет null.

#### 1.

![](./images/ex1.png)

#### 2.

![](./images/ex2.png)  
В этом примере у нас есть одна очередь и два консьюмера.
При этом сообщения равномерно распределяются между консьюмерами.
Так например

```java
 for(int i=0;i< 10;i++){
        template.convertAndSend("queue");
        }
```

раскидает сообщения поровну между С1 и С2.

#### 3.

![](./images/ex3.png)  
В этом же примере сообщения отправляются не сразу в очередь
а в exchange (FanoutExchange), который уже раскидывает сообщения по очередям.
Тут одно и то же сообщение приходит сразу двум консьюмерам. Предыдущий
пример с циклом раскидал бы 10 сообщений С1 и 10 сообщений С2.

#### 4.

![](./images/ex4.png)
В этом примере используется DirectExchange с routing key. Routing key - ключ, по которому exchange будет понимть в
какую очередь отправить сообщение.

#### 5.

![](./images/ex5.png)
То же самое что и в предыдущем примере, но благодаря TopicExchange теперь можно использовать паттерны.  
Например: для routingKey = `*.rabbit.*` будет маппиться `org.rabbit.mq`. Для `org.#` будет
маппится `org.hibernate.Driver`.

#### 6.

![](./images/ex6.png)

```text
    String response = (String) template.convertSendAndReceive("query-example-6",message);
```

```java
    @RabbitListener(queues = "query-example-6")
public String worker1(String message)throws InterruptedException{
        logger.info("received on worker : "+message);
        Thread.sleep(3000); // эмулируем полезную работу
        return"received on worker : "+message;
        }
```

### Микросервисы

Монолит
![](images/monolith.png)

Микросервисная архитектура - набор приложений, которые предоставляют микросервисы и объединены в виртуальное
облако (cloud) - таким образом работают как единое приложение.

- CI/CD - основная цель - ускорение обновления ПО и доставка до конечного потребителя
- pipeline - трубопровод, по которому проходит обновление до конечного пользователя

![](images/ms.svg)

- Service Discovery - паттерн проектирования, организовывающий работу с веб сервисами.
- Spring Cloud Service Discovery - реестр микросервисов (сервер). Популярное решение - Eureka Netflix.

![](images/service_discovery.png)

![](images/plan.png)

- eureka-server - контейнер микросервисов
- eureka-client - поставщик микросервисов, который будет публиковать на eureka-server микросервисы

- API Gateway (AG) - шлюз, который обрабатывает входящие запросы и перекидывает их на нужный мс (с логированием,
  кэшированием, безопасностью и прч.)
- Зачем нужен AG, если есть Eureka Server ?
    - Eureka - реестр микросервисов, а AG - это маршрутизатор запросов (он сам не хранит микросервисы, а только
      перенаправляет запросы)

![](images/ag.png)

- Load Balancer (LB) - балансировщик нагрузки - перенаправляет запросы на нужный экземпляр приложения (если их
  запущено несколько)
- В Spring API Gateway существует автоматическая работа LB, который сам принимает решение, куда перенаправить
  запрос.

![](images/load_balancer.png)

### Безопасность микросервисов

![](images/auth-ms.jpg)
Как видно на картинке, дизайн состоит из трех служб: единой точки входящих запросов от пользователей, реализующей
Gateway API, IDP сервера (Identity Provider), который аутентифицирует пользователей и выдает токен доступа и сервера
ресурсов, который отдает данные. Входящий запрос, не прошедший проверку подлинности, поступает на Gateway и инициирует
authorization flow. Gateway делегирует управление учетными записями пользователей и авторизацию IDP серверу. IDP сервер
проверяет учетную запись пользователя и возвращает на Gateway токен доступа. Gateway прикрепляет токен к запросу
пользователя и отправляет на сервер ресурса. Сервер ресурсов получает от IDP сервера открытый ключ для самостоятельной
валидации токена и в случае успешной валидации возвращает запрашиваемые данные.

# JVM

## Java class file

Из чего состоит

* Constant pool (final)
    - числа, строки
    - указатели на классы, методы, поля
* Описание класса
    * имя
    * модификаторы
    * супер класс
    * супер интерфейс
    * поля, методы, атрибуты

## Java bytecode

JVM - стековая машина и для описания программы требуется три сущности

* Массив инструкций
* Стэк операндов инструкций метода
* Массив локальных переменных(аргументы метода, локальные переменные)

![img.png](images/bytecode1.png)
![img.png](images/bytecode2.png)
![img.png](images/bytecode3.png)
![img.png](images/bytecode4.png)
![img.png](images/bytecode5.png)

## Программа для JVM

Любая программа исполняемая на JVM имеет:

- main class (`public static void main ...`)
- classpath - список директорий и архивов (jar файлов), чтобы JVM могла найти нужные классы

## Java Runtime

Для исполнения программы на JVM одной JVM недостаточно.
Нужен Java Runtime Environment:

- JVM
- Платформенные классы
    - core классы (Object, String)
    - Java standard APIs (IO, NIO, AWT)

## Анатомия JVM

![img.png](images/jvm_scheme.png)

### Загрузка классов

Где JVM берет классы для исполнения:

- Из Java Runtime (платформенные)
- Из classpath приложения
- Само приложение может предоставить для JVM классы посредством `ClassLoader`

* Каждый класс грузится каким-то загрузчиком классов:
    * Платформенные классы - BootstrapClassLoader
    * Классы из classpath - AppClassLoader
    * Классы приложения могут создавать свои загрузчики, которые будут грузить классы

* Загрузчик классов образует уникальное пространство имен классов (Название класса + Название ClassLoader)

### Старт JVM

- Грузится main класс системным загрузчиком (из classpath приложения)
    - Провоцирует загрузку части платформенных классов, т.к загрузчики образуют иерархию, где самый главный bootstrap
- Исполняется метод `main(String[] args)`

### Процесс загрузки класса (создание класса)

- Читается class file
    - Проверяет корректность формата (может выбросить ClassFormatError)
    - Создается ран-тайм представление класса в выделенной области памяти
        - runtime constant pool (Meta Space)
    - Грузятся суперкласс, суперинтерфейс

### Верификация байт-кода

- Происходит с классом один раз
- Проверка корректности инструкций
- Проверка выхода за пределы стэка операндов и локальных переменных
- Проверка совместимости типов

### Инициализация класса

- Вызов статического инициализатора класса
- Случается при первом использовании когда:
    - new
    - доступ до статического поля
    - вызов статического метода
- Провоцирует инициализацию супер-класса и суперинтерфейсов с default методами

## Исполнение Java байт-кода

JVM может исполнять байт-код двумя способами:

- Интерпретировать
- Транслировать в машинный код, который будет исполняться непосредственно на CPU.

### Компиляторы

- Динамические (Just-In-Time JIT)
    - Трансляция в машинный код происходит во время исполнения программы.
- Статические (Ahead-Of-Time AOT)
    - Трансляция происходит до исполнения программы

Динамические компиляторы

- Работают одновременно с исполняемой программой
- Компилируют горячий код (код, который часто исполняется)
- Горячий код вычисляется с помощью динамического профилировщика
- Используют информацию времени исполнения для оптимизаций

Статические компиляторы (AOT)

- Не ограничены в ресурсах для оптимизации программ
- Комиплируют каждый метод программы применяя самые агрессивные оптимизации
- На оптимизацию не тратятся ресурсы во время исполнения программы (быстрее старт)

### Java Native Interface (JNI)

- Связывает JVM с внешним миром (OS)
- Реализуется в JVM как доступ к Meta space
- С помощью JNI написаны платформенно-зависимые реализации Java SE API: AWT, IO, NET

### Threading and synchronization

`java.lang.Thread`

- Java поток мапится на нативный поток в соотношении 1-1
- С потоком связана память используемая для локальных переменных и стэка операндов методов(фреймов методов): стэк
- Имеет информацию о стэке вызовов методов потока (stack trace)
    - В любой момент может о нем рассказать

### Потоки и Java Memory Model

```
// Thread 1:                    
Shared.data = getData();        
Shared.ready = true;

// Thread 2:
while (!Shared.ready) {
    // wait
}

data = Shared.data;
```

JVM может перенаначить порядок выполнения вышеприведенного кода для оптимизации и это может вызывать
ошибки. И дабы явно задать порядок используется модификатор volatile

## Сборка мусора

### Выделение памяти

- Реализация оператора new

- Объекты выделенные с помощью оператора `new` располагаются в heap (куче)

### Аллокация объектов

Означает выделение блока памяти, чаще всего в куче.

- Должна быть быстрой
    - JVM запрашивает память не под один объект, а сразу на много
    - Аллокация методом продвижения границы
- Потоко-безопасной (thread-safe), но при этом не паралелльной (не блокирующей)
    - Thread Local heaps: каждый поток грызет свой кусок памяти

### Структура Java объекта (Layout)

Требует:

- Java Object header
    - указатель на класс
    - монитор (lock)
- Поля

### Мусор

Мусором являются объекты, которые не могут использоваться программой

Не мусор:

- Объекты в статических полях класса
- Объекты доступные со стэка Java потоков
- Объекты, на которые ссылаются живые объекты

### Виды сборок мусора

- Mark-and-sweep
    - Помечает живые объекты (mark), удаляет мусор (sweep)
- Stop-and-copy
    - Копирует живые объекты в специальное место (copy)
    - Освободившееся место может использоваться для новой аллокации

### Stop the World

- Чтобы собрать мусор в общем случае нужно оставновить потоки, чтобы определить где мусор (SWT pause)
- Остановка может быть очень долгой, поэтому нужно уменьшить паузу. Методы:
    - собирать не весь мусор, а часть
    - собирать мусор не в одном а нескольких потоках
    - собирать мусор одновременно с работой программы

### Поколенная сборка мусора

**Слабая гипотеза о поколениях**:

- _большинство объектов умирает молодыми_
- _старые объекты редко ссылаются на молодые_

Это позволяет более эффективно реализовать сборку мусора, разделив java heap на две части

1. Регион, где аллоцируются объкты (young generation)
2. Часть кучи, где хранятся долгоживущие объекты

Поколенный GC:

- удаляет мусор среди молодых объектов
- объекты, пережившие одну или несколько сборок, перемещаются в область старого поколения

## Manageability and Monitoring

JVM знает про нашу программу все:

- про все загруженные классы
- про все живые объекты
- про все потоки
- про все исполняемые методы потоков

Почему бы не поделиться с нами этой информацией во время исполнения ?

JVM Tool Interface (JVM TI)

- отладчики
- профилировщики

Java Management Beans:

- Инструменты мониторинга запущенных приложений
    - JConsole, JMX console, AMC

### Резюмируя

- На вход в JVM приходит bytecode или реализация нативных методов
- bytecode может пойти на исполнение в JVM непосредственно или сначала быть скомпилированным в машинный код и код будет
  испольняться на процессоре
- При загрузке классов тот код который не был скомпилирован заранее будет либо компилироваться либо интерпретироваться
  динамическим компилятором в машинный код
- При исполнении программы у нас выделяется память, она автоматически собирается, все это работает в нескольких потоках
- У нас есть доступ ко всем данным через Mata information

### Реализации JVM

- Oracle HotSpot
- IBM J9
- Excelsior JET

### Плюсы Java

- типизированность байт-кода позволяет эффективно выполнять на JVM так же как и низкоуровневые языки
- динамические фичи как reflection и загрузка дают платформе гибкость

![img.png](images/jdk.png)

## Заключение

### Архитектура JVM

- **ClassLoader** - динамическая загрузка `.class` файлов в JVM и их сохранения в `области метода` JVM.
    - `Bootstrap ClassLoader` - основные библиотеки Java
    - `Extension ClassLoader` - дочерний к Bootstrap, загружает расширения основных классов Java
    - `System ClassLoader` - классы, найденные в переменной среды `CLASSPATH` или параметре -cp в командной строке
- **Область памяти**
    - `Область метода` - является общей для всех потоков. Она хранит структуры для каждого класса, такие как
      пул констант, данные полей и методов, а также код для методов и конструкторов. Хотя область метода является
      логически
      частью кучи, простые реализации могут не обрабатываться сборщиком мусора. Область метода может быть увеличена или
      сокращена в зависсимости от требований.
    - `Область кучи` - В куче хранятся все объекты, на которые ссылаются переменные из стэка. Для каждого запущенного
      процесса JVM существует только одна область памяти в куче. Следовательно, эта общая часть памяти одна независимо
      от
      того, сколько потоков выполняется.
    - `Область стэка` - отвечает за хранение ссылок на объекты кучи и за хранение примитивов. Кроме того, переменные в
      стэке имеют область видимости. Если предположить, что нет глобальных переменных, а только локальные переменные,
      если
      компилятор выполняет тело метода, то он можеи получить доступ только к объектам из стэка, которые находятся внутри
      тела метода. Он не может получить доступ к другим локальным переменным, так как они не входят в область видимости.
      Когда метод завершается и возвращается, верхняя часть стэка выталкиывается, и активная область видимости
      изменяется.
      Стэковая память выделяется для каждого потока.
      ![img.png](images/stack_and_heap.png)
    - `PCR (Program Counter Register)` - Каждый поток JVM имеет свой собственный регистр PC. В любой момент каждый поток
      JVM выполняет текущий метод для данного потока. PCR содержит адрес инструкции JVM, выполняемой в настоящее время.
    - `Стеки нативных методов` - Нативные методы - методы, написанные на C.
- **Исполнительный механизм** - если байт-код отправился сразу в JVM (runtime без преобразования в машинный код) то этот
  механизм будет считывать байт-код и выполнять по частям )
    - `Интерпретатор` - интерпретирует байт-код быстро, но выполняется медленно. Недостаток интерпретатора заключается в
      том, что, когда один метод вызывается несколько раз, то требуется новая интерпретация.
    - `JIT Compiler` устраняет недостатки интерпретатора. В тот момент, когда в сценарии обнаруживается повторяющийся
      код, к нему подключается `JIT-компилятор` для ускорения операции. Затем он компилирует байт-код и заменяет его
      собственным кодом. Такой процесс повышает производительность всей системы.
    - JVM содержит внутри себя и интерпретатор, и транслятор (just-in-time translator, JIT).
      Сначала программа исполняется интерпретатором, и для каждого метод накапливается статистика, сколько раз он
      вызывался. Как только обнаружатся часто исполняемые методы, они транслируются в машинный код, а редко исполняемые
      продолжают интерпретироваться. Этот процесс идет постоянно, самые часто исполняемые методы подвергаются
      перекомпиляции с многоуровневой оптимизацией, чтобы сделать код максимально эффективным.
- **Сборщик мусора**

## Java core

### Чем абстрактный класс отличается от интерфейса? В каких случаях следует использовать абстрактный класс, а в каких интерфейс?

Абстрактные классы содержат частичную реализацию, которая дополняется или расширяется в подклассах.
При этом все подклассы схожи между собой в части реализации, унаследованной от абстрактного класса,
и отличаются лишь в части собственной реализации абстрактных методов родителя. Поэтому абстрактные классы
применяются в случае построения иерархии однотипных, очень похожих друг на друга классов. В этом случае наследование
от абстрактного класса, реализующего поведение объекта по умолчанию может быть полезно, так как позволяет избежать
написания повторяющегося кода. Во всех остальных случаях лучше использовать интерфейсы.

### Для чего в Java используются статические блоки инициализации?
Статические блоки инициализация используются для выполнения кода, который должен выполняться один раз при инициализации класса загрузчиком классов, в момент, предшествующий созданию объектов этого класса при помощи конструктора. Такой блок (в отличие от нестатических, принадлежащих конкретном объекту класса) принадлежит только самому классу (объекту метакласса `Class`).

### Может ли статический метод быть переопределён или перегружен?
Перегружен - да. Всё работает точно так же, как и с обычными методами - 2 статических метода могут иметь одинаковое имя, если количество их параметров или типов различается.

Переопределён - нет. Выбор вызываемого статического метода происходит при раннем связывании (на этапе компиляции, а не выполнения) и выполняться всегда будет родительский метод, хотя синтаксически переопределение статического метода - это вполне корректная языковая конструкция.

В целом, к статическим полям и методам рекомендуется обращаться через имя класса, а не объект.

### Могут ли нестатические методы перегрузить статические?
Да. В итоге получится два разных метода. Статический будет принадлежать классу и будет доступен через его имя, а нестатический будет принадлежать конкретному объекту и доступен через вызов метода этого объекта.

### Можно ли сузить уровень доступа/тип возвращаемого значения при переопределении метода?
 - При переопределении метода нельзя сузить модификатор доступа к методу (например с public в MainClass до private в Class extends MainClass).
 - Изменить тип возвращаемого значения при переопределении метода нельзя, будет ошибка attempting to use incompatible return type.
 - Можно сузить возвращаемое значение, если они совместимы.

### Возможно ли при переопределении метода изменить: модификатор доступа, возвращаемый тип, тип аргумента или их количество, имена аргументов или их порядок; убирать, добавлять, изменять порядок следования элементов секции throws?

При переопределении метода сужать модификатор доступа не разрешается, т.к. это приведёт к нарушению принципа подстановки Барбары Лисков. Расширение уровня доступа возможно.

Можно изменять все, что не мешает компилятору понять какой метод родительского класса имеется в виду:

 - Изменять тип возвращаемого значения при переопределении метода разрешено только в сторону сужения типа (вместо родительского класса - наследника).
 - При изменении типа, количества, порядка следования аргументов вместо переопределения будет происходить overloading (перегрузка) метода.
 - Секцию throws метода можно не указывать, но стоит помнить, что она остаётся действительной, если уже определена у метода родительского класса. Так же, возможно добавлять новые исключения, являющиеся наследниками от уже объявленных или исключения RuntimeException. Порядок следования таких элементов при переопределении значения не имеет.

### Можно ли объявить метод абстрактным и статическим одновременно?
Нет. В таком случае компилятор выдаст ошибку: "Illegal combination of modifiers: ‘abstract’ and ‘static’". Модификатор `abstract` говорит, что метод будет реализован в другом классе, а `static` наоборот указывает, что этот метод будет доступен по имени класса.

### Где разрешена инициализация статических/нестатических полей?
 - Статические поля можно инициализировать при объявлении, в статическом или нестатическом блоке инициализации.
 - Нестатические поля можно инициализировать при объявлении, в нестатическом блоке инициализации или в конструкторе.

### Какие типы классов бывают в java?
 - Top level class (Обычный класс):
   - Abstract class (Абстрактный класс);
   - Final class (Финализированный класс).
 - Interfaces (Интерфейс).
 - Enum (Перечисление).
 - Nested class (Вложенный класс):
   - Static nested class (Статический вложенный класс);
   - Member inner class (Простой внутренний класс);
   - Local inner class (Локальный класс);
   - Anonymous inner class (Анонимный класс).

### Расскажите про вложенные классы. В каких случаях они применяются?
Класс называется вложенным (Nested class), если он определен внутри другого класса. Вложенный класс должен создаваться только для того, чтобы обслуживать обрамляющий его класс. Если вложенный класс оказывается полезен в каком-либо ином контексте, он должен стать классом верхнего уровня. Вложенные классы имеют доступ ко всем (в том числе приватным) полям и методам внешнего класса, но не наоборот. Из-за этого разрешения использование вложенных классов приводит к некоторому нарушению инкапсуляции.

Существуют четыре категории вложенных классов:

 - Static nested class (Статический вложенный класс);
 - Member inner class (Простой внутренний класс);
 - Local inner class (Локальный класс);
 - Anonymous inner class (Анонимный класс).
 - 
Такие категории классов, за исключением первого, также называют внутренними (Inner class). Внутренние классы ассоциируются не с внешним классом, а с экземпляром внешнего.

Каждая из категорий имеет рекомендации по своему применению. Если вложенный класс должен быть виден за пределами одного метода или он слишком длинный для того, чтобы его можно было удобно разместить в границах одного метода и если каждому экземпляру такого класса необходима ссылка на включающий его экземпляр, то используется нестатический внутренний класс. В случае, если ссылка на обрамляющий класс не требуется - лучше сделать такой класс статическим. Если класс необходим только внутри какого-то метода и требуется создавать экземпляры этого класса только в этом методе, то используется локальный класс. А, если к тому же применение класса сводится к использованию лишь в одном месте и уже существует тип, характеризующий этот класс, то рекомендуется делать его анонимным классом.

### Что такое «статический класс»?
Это вложенный класс, объявленный с использованием ключевого слова static. К классам верхнего уровня модификатор static неприменим.

### Какие существуют особенности использования вложенных классов: статических и внутренних? В чем заключается разница между ними?

 - Вложенные классы могут обращаться ко всем членам обрамляющего класса, в том числе и приватным.
 - Для создания объекта статического вложенного класса объект внешнего класса не требуется.
 - Из объекта статического вложенного класса нельзя обращаться к не статическим членам обрамляющего класса напрямую, а только через ссылку на экземпляр внешнего класса.
 - Обычные вложенные классы не могут содержать статических методов, блоков инициализации и классов. Статические вложенные классы - могут.
 - В объекте обычного вложенного класса хранится ссылка на объект внешнего класса. Внутри статической такой ссылки нет. Доступ к экземпляру обрамляющего класса осуществляется через указание .this после его имени. Например: Outer.this.

inner class
```java
public class MyClass {
    public static void main(String args[]) {
        Outer outer = new Outer();
        Outer.Inner inner = outer.new Inner(5);
        inner.printSomeStr();
    }
}

class Outer {
    
    private String str = "Hello world";
    
 
    class Inner {
        private int a;
        private static String someStr = "Static variable";
        
        public Inner(int a) {
            this.a = a;
            System.out.println(Outer.this.str); // Hello world
        }
        
        public static void printSomeStr() {
            System.out.println(someStr); // Static variable
        }
        
        
        public int getA() { return this.a; }
    }
}
```

private inner class
```java
public class MyClass {
    public static void main(String args[]) {
        Outer outer = new Outer();
        Outer.Inner inner = outer.new Inner(5); // MyClass.java:4: error: Outer.Inner has private access in Outer
    }
}

class Outer {
    
    private String str = "Hello world";
    
 
    private class Inner {
        private int a;
        private static String someStr = "Static variable";
        
        public Inner(int a) {
            this.a = a;
            System.out.println(Outer.this.str); // Hello world
        }
        
        public static void printSomeStr() {
            System.out.println(someStr); // Static variable
        }
        
        
        public int getA() { return this.a; }
    }
}
```

### Что такое Heap и Stack память в Java? Какая разница между ними?

**Heap** (куча) используется Java Runtime для выделения памяти под объекты и классы. Создание нового объекта также происходит в куче. Это же является областью работы сборщика мусора. Любой объект, созданный в куче, имеет глобальный доступ и на него могут ссылаться из любой части приложения.

**Stack** (стек) это область хранения данных также находящееся в общей оперативной памяти (RAM). Всякий раз, когда вызывается метод, в памяти стека создается новый блок, который содержит примитивы и ссылки на другие объекты в методе. Как только метод заканчивает работу, блок также перестает использоваться, тем самым предоставляя доступ для следующего метода. Размер стековой памяти намного меньше объема памяти в куче. Стек в Java работает по схеме LIFO (Последний-зашел-Первый-вышел)

Различия между Heap и Stack памятью:

 - Куча используется всеми частями приложения, в то время как стек используется только одним потоком исполнения программы.
 - Всякий раз, когда создается объект, он всегда хранится в куче, а в памяти стека содержится лишь ссылка на него. Память стека содержит только локальные переменные примитивных типов и ссылки на объекты в куче.
 - Объекты в куче доступны с любой точки программы, в то время как стековая память не может быть доступна для других потоков.
 - Стековая память существует лишь какое-то время работы программы, а память в куче живет с самого начала до конца работы программы.
 - Если память стека полностью занята, то Java Runtime бросает исключение java.lang.StackOverflowError. Если заполнена память кучи, то бросается исключение java.lang.OutOfMemoryError: Java Heap Space.
 - Размер памяти стека намного меньше памяти в куче.
 - Из-за простоты распределения памяти, стековая память работает намного быстрее кучи.
 - Для определения начального и максимального размера памяти в куче используются -Xms и -Xmx опции JVM. Для стека определить размер памяти можно с помощью опции -Xss.

### Верно ли утверждение, что примитивные типы данных всегда хранятся в стеке, а экземпляры ссылочных типов данных в куче?

Не совсем. Примитивное поле экземпляра класса хранится не в стеке, а в куче. Любой объект (всё, что явно или неявно создаётся при помощи оператора new) хранится в куче.

### Сборка мусора подробно
Young generation делится на три части: Eden, To space, From space. Большинство объектов создаются в Eden, за исключением очень больших объектов, которые сразу помещаются в Old generation. В Survivor spaces (To space, From space) помещаюься объекты, пережившие по крайней мере одну сборку, но еще не достигли порога старости, чтобы быть помещеными в Old generation.

Когда Young generation переполняется, то запускается механизм легкой сборки (не всей кучи). Вначале работы To space - пуст, а From space содержит объекты, пережившие предыдущие сборки. Сборщик мусора ищет живые объекты в Eden и копирует в To space, а также ищет в From space объекты, не пережившие заданное кол-во сборок. Старые объекты из From space перемещаются в Old generation. И после From space и To space меняются ролями, а Old generation увеличивается. Этот алгоритм называется `copying`.

В Old generation используется алгоритм `mark-sweep-compact`. В фазе Mark сборщик мусора помечает все живые объекты, затем в фазе Sweep, все непомеченные объекты удаляются, а в фазе Compact все живые объекты перемещаются в начало Old generation, в результате чего свободная память после очистки представляет собой непрерывную область.

### Что такое «пул строк»?

Пул строк – это набор строк, хранящийся в Heap.

 - Пул строк возможен благодаря неизменяемости строк в Java и реализации идеи интернирования строк;
 - Пул строк помогает экономить память, но по этой же причине создание строки занимает больше времени;
 - Когда для создания строки используются ", то сначала ищется строка в пуле с таким же значением, если находится, то просто возвращается ссылка, иначе создается новая строка в пуле, а затем возвращается ссылка на неё;
 - При использовании оператора new создаётся новый объект String. Затем при помощи метода intern() эту строку можно поместить в пул или же получить из пула ссылку на другой объект String с таким же значением;
 - Пул строк является примером паттерна «Приспособленец» (Flyweight).

```text
System.out.println("Hello world" == "Hello " + "world"); // true
System.out.println(new String("Hello").intern() == new String("Hello").intern()); // true
```

### Что такое finalize()? Зачем он нужен?
Через вызов метода finalize() (который наследуется от Java.lang.Object) JVM реализуется функциональность аналогичная функциональности деструкторов в С++, используемых для очистки памяти перед возвращением управления операционной системе. **Данный метод вызывается при уничтожении объекта сборщиком мусора** (garbage collector) и переопределяя finalize() можно запрограммировать действия необходимые для корректного удаления экземпляра класса - например, закрытие сетевых соединений, соединений с базой данных, снятие блокировок на файлы и т.д.

```java
public class Main {
    public static void main(String[] args) {
    
        Test test = new Test();
        test.a();
        test = null;
        System.gc();
        
        test = new Test();
        test.a();
        
        System.out.println("main() done");
    }
}

class Test {
    public void a() {
        System.out.println("a() is called");
    }
    
    @Override
    protected void finalize() throws Throwable {
        System.out.println("finalize() done");
    }
}
```

Вывод N1:
```text
a() is called
a() is called
main() done
finalize() done
```

Вывод N2:
```text
a() is called
a() is called
finalize() done
main() done
```

Непосредственно вызов finalize() происходит в отдельном потоке `Finalizer` (java.lang.ref.Finalizer.FinalizerThread)

### Приведение типов
```java
abstract class Phone {
    private String name;
    
    public Phone(String name) {
        this.name = name;
    }
    
    public abstract void call();
}

class OldPhone extends Phone {
    
    public OldPhone(String name) {
        super(name);
    }
    
    
    @Override
    public void call() {
        System.out.println("Кручу колесико");
    }
    
    public Object getOldPhone() {
        return this;
    }
}


class SmartPhone extends Phone {
    public SmartPhone(String name) {
        super(name);
    }
    
    @Override
    public void call() {
        System.out.println("Сири, позвони");
    }
}

class User {
    // полиморфизм
    public void call(Phone phone) {
        phone.call();
    }
}

public class Main {
    public static void main(String[] args) {
        User user = new User();
        Phone phone = new SmartPhone("iPhone 14"); // восходящее преобразование (расширение)
        user.call(phone);
        
        
        OldPhone oldPhone = new OldPhone("Old phone");
        OldPhone oldPhone1 = (OldPhone) oldPhone.getOldPhone(); // нисходящее преобразование (сужение)
        oldPhone1.call();
        
    }
}
```

### Что такое литералы?
Литералы — это явно заданные значения в коде программы — константы определенного типа, которые находятся в коде в момент запуска.

### Почему String неизменяемый и финализированный класс?
 - Пул строк возможен только потому, что строка неизменяемая, таким образом виртуальная машина сохраняет больше свободного места в Heap, поскольку разные строковые переменные указывают на одну и ту же переменную в пуле. Если бы строка была изменяемой, то интернирование строк не было бы возможным, потому что изменение значения одной переменной отразилось бы также и на остальных переменных, ссылающихся на эту строку.
 - Если строка будет изменяемой, тогда это станет серьезной угрозой безопасности приложения. Например, имя пользователя базы данных и пароль передаются строкой для получения соединения с базой данных и в программировании сокетов реквизиты хоста и порта передаются строкой. Так как строка неизменяемая, её значение не может быть изменено, в противном случае злоумышленник может изменить значение ссылки и вызвать проблемы в безопасности приложения.
 - Неизменяемость позволяет избежать синхронизации: строки безопасны для многопоточности и один экземпляр строки может быть совместно использован различными потоками.
 - Строки используются classloader и неизменность обеспечивает правильность загрузки класса.
 - Поскольку строка неизменяемая, её hashCode() кэшируется в момент создания и нет необходимости рассчитывать его снова. Это делает строку отличным кандидатом для ключа в HashMap т.к. его обработка происходит быстрее.

### Какая основная разница между String, StringBuffer, StringBuilder?
Класс `String` является неизменяемым (immutable) - модифицировать объект такого класса нельзя, можно лишь заменить его созданием нового экземпляра.

Класс `StringBuffer` изменяемый - использовать StringBuffer следует тогда, когда необходимо часто модифицировать содержимое.

Класс `StringBuilder` был добавлен в Java 5 и он во всем идентичен классу `StringBuffer` за исключением того, что он не синхронизирован и поэтому его методы выполняются значительно быстрей.

### Что такое класс Object? Какие в нем есть методы?
Object это базовый класс для всех остальных объектов в Java. Любой класс наследуется от Object и, соответственно, наследуют его методы:

public boolean equals(Object obj) – служит для сравнения объектов по значению;

int hashCode() – возвращает hash код для объекта;

String toString() – возвращает строковое представление объекта;

Class getClass() – возвращает класс объекта во время выполнения;

protected Object clone() – создает и возвращает копию объекта;

void notify() – возобновляет поток, ожидающий монитор;

void notifyAll() – возобновляет все потоки, ожидающие монитор;

void wait() – остановка вызвавшего метод потока до момента пока другой поток не вызовет метод notify() или notifyAll() для этого объекта;

void wait(long timeout) – остановка вызвавшего метод потока на определённое время или пока другой поток не вызовет метод notify() или notifyAll() для этого объекта;

void wait(long timeout, int nanos) – остановка вызвавшего метод потока на определённое время или пока другой поток не вызовет метод notify() или notifyAll() для этого объекта;

protected void finalize() – может вызываться сборщиком мусора в момент удаления объекта при сборке мусора.

### HashMap
![img.png](images/hashmap.png)

HashMap состоит из «корзин» (bucket). С технической точки зрения «корзины» — это элементы массива, которые хранят ссылки на списки элементов. При добавлении новой пары «ключ-значение», вычисляет хэш-код ключа, на основании которого вычисляется номер корзины (номер ячейки массива), в которую попадет новый элемент. Если корзина пустая, то в нее сохраняется ссылка на вновь добавляемый элемент, если же там уже есть элемент, то происходит последовательный переход по ссылкам между элементами в цепочке, в поисках последнего элемента, от которого и ставится ссылка на вновь добавленный элемент. Если в списке был найден элемент с таким же ключом, то он заменяется.

## Многопоточность
 * Многопоточность - возможность центрального процессора выполнять несколько потоков или процессов одновременно.
 * В чем отличие потока от процесса 
   * Каждому процессу операционка выделяет память. Память одного процесса не пересекается с памятью другого. Поток может создаваться внутри процесса. Внутри процесса потоки взаимодействуют с одной и той же памятью процесса
 * Как ЦП работает с потоками
   * ![img.png](images/img.png).
     * На картинке пример где у ЦП всего одно ядро и мы создаем 2 потока. ЦП выделяет кванты времени для каждого потока. На самом деле потоки не выполняются паралелльно. ЦП просто создает иллюзию выделяя очень маленькие кванты времени
   * ![img.png](img.png).
     * На этой картинке у нас ЦП имеет 2 ядра. Тогда мы уже действительно можем параллельно запустить два потока. 
 * Преимущества
   * Паралелльно запускать несколько операций
 * Недостатки
   * Сложно обнаружить ошибку синхронизации
   * Создание потока - дорогая операция. Переключение между потоками - тоже.
 * Создание и старт потока
   * Когда вызываем метод `start()`, а не `run()` - это пример паттерна "Шаблонный метод".

```java
import java.util.stream.IntStream;

// Создание потоков внутри потока
class Runner {
    private static final int THREADS_AMOUNT = 10;

    public static void main(String[] args) {
        final Runnable displayThreadNames = () -> System.out.println(Thread.currentThread().getName());
        final Runnable createThreads = () -> {
            IntStream.range(0, THREADS_AMOUNT)
                    .forEach(i -> startThread(displayThreadNames));
        };
        
        startThread(createThreads);
    }
    
    public static void startThread(Runnable runnable) {
        Thread thread = new Thread(runnable);
        thread.start();
    }
}
```

![img_1.png](img_1.png)

### Thread::join()
Приостанавливает работу текущего потока, пока поток, на котором был вызван `join()`, не прекратит работу.

В этом примере расчитвается арифметическая прогрессия чисел от 1 до 1000 в двух потоках.

```java
public class Runner {
    public static void main(String[] args) throws InterruptedException {

        SummingThread summingThread1 = new SummingThread(1, 500);
        Thread task1 = new Thread(summingThread1);
        task1.start();

        SummingThread summingThread2 = new SummingThread(501, 1000);
        Thread task2 = new Thread(summingThread2);
        task2.start();

        task1.join();
        task2.join();

        int result = summingThread1.getResult() + summingThread2.getResult();
        System.out.println(result + ": " + Thread.currentThread().getName());

    }
    
    private static class SummingThread implements Runnable {
        private int result = 0;
        private int from;
        private int to;

        public SummingThread(int from, int to) {
            this.from = from;
            this.to = to;
        }

        public int getResult() {
            return result;
        }

        @Override
        public void run() {
            IntStream.rangeClosed(from, to).forEach(i -> result += i);
            System.out.println(result + ": " + Thread.currentThread().getName());
        }
    }
}
```

### Состояния потоков
 - NEW
 - RUNNABLE
 - BLOCKED
 - WAITING
 - TIMEDWAITING (Временно ожидающий)
 - TERMINATED (Завершенный)

![img_4.png](img_4.png)

Вариант deadlock'а 
```java
public class Runner {
    public static void main(String[] args) throws InterruptedException {
        Thread mainThread = Thread.currentThread();

        Thread someThread = new Thread(() -> {
            try {
                mainThread.join(); // поток someThread ждет окончания работы потока mainThread
                System.out.println(Thread.currentThread().getState());

            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });

        someThread.start();

        someThread.join(); // поток mainThread ждет окончания работы someThread
        
        System.out.println(someThread.getState());
        
    }
}
```


WAITING и RUNNABLE
```java
public class Runner {
    public static void main(String[] args) throws InterruptedException {
        Thread mainThread = Thread.currentThread();

        Thread someThread = new Thread(() -> {
            try {
                mainThread.join();
                System.out.println(Thread.currentThread().getState()); // RUNNABLE

            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });

        someThread.start();
        Thread.sleep(100L);

        System.out.println(someThread.getState()); // WAITING
    }
}
```

TIMED_WAITING

```java
public class Runner {
    public static void main(String[] args) throws InterruptedException {
        Thread mainThread = Thread.currentThread();

        Thread someThread = new Thread(() -> {
            try {
                mainThread.join(1000L); // будет ждать mainThread не более секунды
                System.out.println(Thread.currentThread().getState()); // RUNNABLE

            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });

        someThread.start();
        Thread.sleep(500L); // но mainThread завершился через пол секунды

        System.out.println(someThread.getState()); // TIMED_WAITING
    }
}
```

TERMINATED

```java
public class Runner {
    public static void main(String[] args) throws InterruptedException {
        Thread mainThread = Thread.currentThread();

        Thread someThread = new Thread(() -> {
            try {
                mainThread.join(500L);
                System.out.println(Thread.currentThread().getState()); // RUNNABLE

            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });

        someThread.start();
        Thread.sleep(1000L);

        System.out.println(someThread.getState()); // TERMINATED
    }
}
```

Если в одном возникло исключение, то остальные будут работать в штатном режиме.

### InterruptedException
Возникает, когда один поток вызывает метод `interrupt()` у заблокированного потока (Например, который спит)

### Приоритеты потоков
Приоритеты бывают от 1 до 10 включительно, где 10 - наивысший. По умолчанию - это 5. При создании потока, приоритет наследуется от родительского. Нет гарантий, что при манипуляции приоритетов получиться запускать их в задуманном порядке.

### Потоки-демоны
Потоки демоны завершают свою работу когда поток вызвавший их сам завершает работу. Обычные потоки-родители ждут своих потоков-детей. Так же статус потока наследуется потомкам.
#### Небольшой пример того, в каком случае блок `finally` не выполнится
```java
public class Runner {
    public static void main(String[] args) throws InterruptedException {
        Thread daemonThread = new Thread(() -> {
            try {
                System.out.println("Daemon thread");
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            } finally {
                System.out.println("Finally");
            }
        });

        daemonThread.setDaemon(true);
        daemonThread.start();

        TimeUnit.SECONDS.sleep(1);
    }
}

```

### `Thread.UncaughtExceptionHandler`
Обработчик необрабатываемых исключений

```java
public class Runner {
    public static void main(String[] args) throws InterruptedException {

        Thread.setDefaultUncaughtExceptionHandler((thread, exception) -> {
            System.out.println(thread.getName() + ": " + exception.getMessage());
        });

        new Thread(() -> {
            throw new IllegalArgumentException("Illegal Argument");
        }).start();

        new Thread(() -> {
            throw new IllegalArgumentException("Illegal Argument x 2");
        }).start();
        
    }
}
```

### Фабрика потоков
```java
public static class CustomThreadFactory implements ThreadFactory {

    private final Thread.UncaughtExceptionHandler handler;

    public CustomThreadFactory(Thread.UncaughtExceptionHandler handler) {
        this.handler = handler;
    }

    @Override
    public Thread newThread(Runnable runnable) {
        Thread thread = new Thread(runnable);
        thread.setUncaughtExceptionHandler(handler);
        thread.setDaemon(true);

        return thread;
    }
}
```

### Синхронизация 
Каждый поток имеет свою стековую память. Однако потоки взаимодействуют с одной и той же кучей
![img_5.png](img_5.png)

#### Атомарные операции
![img_6.png](img_6.png)

Если операция атомарня (A и C), то планировщик задач не может прервать выполнение этой операции, выделив квант времени другому потоку. Атомарная операция должна быть выполнена полностью или не выполнена вовсе.

В Java атомарные операции
 - присваивание со всеми примитивными типами (кроме double и long)
 - присваивание ссылок

**Почему инкремент не атомарная операция:** 

![img_7.png](img_7.png)

![img_8.png](img_8.png)

#### Synchronized
![img_9.png](img_9.png)

Нам нужно сделать так, чтобы операция инкремент была атомарная.

```java
public class Runner {

    public static int counter = 0;

    public static void main(String[] args) throws InterruptedException {


        Thread thread1 = new Thread(() -> IntStream.range(0, 500).forEach(i -> incrementAndGet()));
        Thread thread2 = new Thread(() -> IntStream.range(0, 500).forEach(i -> incrementAndGet()));

        thread1.start();
        thread2.start();

        thread1.join();
        thread2.join();

        System.out.println(counter);
    }

    public static synchronized void incrementAndGet() {
        counter = counter + 1;
    }
}
```

#### Monitor
![img_10.png](img_10.png)

![img_11.png](img_11.png)

Когда мы вешаем `synchronized` на статический методы. То потоки будут конкурировать за монитор класса, в котором эти статические методы находятся.

![img_12.png](img_12.png)

### Lock

#### В каких случаях применять локи
Если в одном методе нужно захватить блокировку, а в другом освободить

Метод `lock()` захватывает блокировку, если она не была захвачена другим потоком. Если уже была захвачена, то метод блокирует текущий поток. Ключевое слово `synchronized` тоде 
блокирует. Если мы не хотим чтобы блокировался поток, можно использовать метод `tryLock`
![img_13.png](img_13.png)

#### Condition
Это что-то типа `wait` и `notify` но у объекта `Lock'a`
Преимущества: представим ситуацию, что у нас есть пять proudcers и пять consumers 
![img_14.png](img_14.png)

Пять producers начали все пихать значения в буфер, заполнив его. Если бы мы работали в связке `synchronized+wait/notify`
то вызывая метод `notify` можно было случайно оживить какой-то продьюсер. Если использовать `notifyAll` то мы будим
вообще всех. В этом и есть преимущество `Condition`. Мы можем создать, например, два `Condition`: один - для того случая,
когда продьюсеры уже не могут писать что-то в буфер (т.е когда буфер полон). Второй - когда консьюмерам нечего читать
(когда буфер пуст). И теперь для случая, когда больше нельзя записывать мы ставим в ожидание на condition `fullCondition.await()`.
А когда место появляется вызываем `fullCondition.signal()`. Аналогично для empty.

### ReadWriteLock
![img_15.png](img_15.png)

Без них, чтение переменной в один квант времени мог выполнять только один поток а все остальные - заблоканы. С ними же все потоки могут 
читать одну переменную. А когда эту переменную какой-то поток модифицирует, читающие потоки будут находиться в заблокированном состоянии.

```
private ReadWriteLock readWriteLock = new ReentrantReadWriteBlock();
private Lock readLock = readWriteLock.readLock();
private Lock writeLock = readWriteLock.writeLock();
```

### Volatile
![img_16.png](img_16.png)

У ядер процессора есть свой локальеый кеш, в котором хранятся наиболее часто используемые инструкции

![img_17.png](img_17.png)

В этом примере первый поток читает переменную и сохраняет значение в свой локальный кеш. Второй поток изменяет эту переменную на 'false'.
При повторном чтении первым потоком, знчение переменной возьмется не из основной памяти, а из локального кеша. Чтобы вручную задать такое 
поведение, при котором переменную не надо кешировать в локальный кеш, а сразу брать из основной памяти, нужно использовать `volatile`

![img_18.png](img_18.png)