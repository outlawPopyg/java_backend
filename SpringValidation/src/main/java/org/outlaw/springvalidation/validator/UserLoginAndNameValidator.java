package org.outlaw.springvalidation.validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.outlaw.springvalidation.UserRequest;
import org.outlaw.springvalidation.constraint.UserConstraint;

import java.util.Objects;

public class UserLoginAndNameValidator implements ConstraintValidator<UserConstraint, UserRequest> {

    private static final String MESSAGE = "User must have only login or name.";

    @Override
    public boolean isValid(UserRequest request, ConstraintValidatorContext context) {
        if (Objects.nonNull(request.getName()) && Objects.nonNull(request.getLogin())) {

            buildConstraintViolationWithTemplate(context, "name");
            return false;
        }

        return true;
    }

    private void buildConstraintViolationWithTemplate(ConstraintValidatorContext context, String fieldName) {
        context.buildConstraintViolationWithTemplate(MESSAGE)
                .addPropertyNode(fieldName)
                .addConstraintViolation()
                .disableDefaultConstraintViolation();
    }

}
