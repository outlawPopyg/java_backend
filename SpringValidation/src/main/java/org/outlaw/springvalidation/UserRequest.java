package org.outlaw.springvalidation;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.outlaw.springvalidation.constraint.UserConstraint;

import java.time.Instant;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@UserConstraint
public class UserRequest {
    private String name;

    private String login;

    @NotBlank(message = "password is null")
    private String password;

    private Instant birthday;

}
