package org.outlaw.springvalidation.controller;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.outlaw.springvalidation.UserRequest;
import org.outlaw.springvalidation.constraint.UserConstraint;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@Slf4j
public class BaseController {


    @PostMapping("/users")
    public ResponseEntity<?> addUser(@RequestBody @Valid UserRequest userRequest) {
        log.info("Creating user");
        return ResponseEntity.ok("User is valid");
    }

    @PostMapping("/users2")
    public ResponseEntity<?> addUser2(@RequestBody @Valid UserRequest userRequest) {
        log.info("Creating user");
        return ResponseEntity.ok("User is valid");
    }


}
