package org.outlaw.springvalidation.constraint;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import org.outlaw.springvalidation.validator.UserLoginAndNameValidator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Constraint(validatedBy = {UserLoginAndNameValidator.class})
@Target({ElementType.TYPE, PARAMETER, FIELD })
@Retention(RUNTIME)
public @interface UserConstraint {
    String message() default "{UserConstraint error}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

