package org.outlaw.springsecurity.security.filter;

import com.auth0.jwt.exceptions.JWTVerificationException;
import lombok.RequiredArgsConstructor;
import org.outlaw.springsecurity.security.utils.AuthorizationHeaderUtil;
import org.outlaw.springsecurity.security.utils.JwtUtil;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.outlaw.springsecurity.security.filter.JwtAuthenticationFilter.AUTHENTICATION_URL;

@Component
@RequiredArgsConstructor
public class JwtAuthorizationFilter extends OncePerRequestFilter { // срабатывает один раз на один запрос

    private final AuthorizationHeaderUtil authorizationHeaderUtil;
    private final JwtUtil jwtUtil;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        if (request.getServletPath().equals(AUTHENTICATION_URL)) { // если запрос был на аутентификацию
            filterChain.doFilter(request, response);
        } else {
            if (authorizationHeaderUtil.hasAuthorizationToken(request)) { // если есть токен в заголовках

                String jwt = authorizationHeaderUtil.getToken(request);

                try {
                    Authentication authentication = jwtUtil.buildAuthentication(jwt);
                    // привязка аутентификации к текущему потоку
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                    filterChain.doFilter(request, response);
                } catch (JWTVerificationException e) {
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                }

            } else {
                filterChain.doFilter(request, response); // если нет -> запрос не авторизован
            }
        }
    }
}
