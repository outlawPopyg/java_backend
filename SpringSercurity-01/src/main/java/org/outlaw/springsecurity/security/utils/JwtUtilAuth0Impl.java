package org.outlaw.springsecurity.security.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.outlaw.springsecurity.models.User;
import org.outlaw.springsecurity.security.details.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.util.*;

@Component
public class JwtUtilAuth0Impl implements JwtUtil {
    private static final long ACCESS_TOKEN_EXPIRES_TIME = 60 * 1000; // one minute
    private static final long REFRESH_TOKEN_EXPIRES_TIME = 3 * 60 * 1000; // three minutes
    @Value("${jwt.secret}")
    private String secretValue;

    @Override
    public Map<String, String> generateTokens(String subject, String authority, String issuer) {
        // Алгоритм, который будет подписывать на основании ключа
        Algorithm algorithm = Algorithm.HMAC256(secretValue.getBytes(StandardCharsets.UTF_8));

        String accessToken = JWT.create()
                .withSubject(subject)
                .withExpiresAt(new Date(System.currentTimeMillis() + ACCESS_TOKEN_EXPIRES_TIME))
                .withClaim("role", authority)
                .withIssuer(issuer)
                .sign(algorithm);

        String refreshToken = JWT.create()
                .withSubject(subject)
                .withExpiresAt(new Date(System.currentTimeMillis() + REFRESH_TOKEN_EXPIRES_TIME))
                .withClaim("role", authority)
                .withIssuer(issuer)
                .sign(algorithm);

        Map<String, String> tokens = new HashMap<>();
        tokens.put("accessToken", accessToken); // для доступа
        tokens.put("refreshToken", refreshToken); // для обновления доступа

        return tokens;
    }

    @Override
    public Authentication buildAuthentication(String token) throws JWTVerificationException {
        ParsedToken parsedToken = parse(token);

        return new UsernamePasswordAuthenticationToken(new UserDetailsImpl(
                User.builder()
                        .role(User.Role.valueOf(parsedToken.role))
                        .email(parsedToken.email)
                        .build()),
                null, List.of(new SimpleGrantedAuthority(parsedToken.role)));
    }

    private ParsedToken parse(String token) throws JWTVerificationException {
        Algorithm algorithm = Algorithm.HMAC256(secretValue.getBytes(StandardCharsets.UTF_8));

        JWTVerifier verifier = JWT.require(algorithm).build();

        DecodedJWT decodedJWT = verifier.verify(token);

        String email = decodedJWT.getSubject();
        String role = decodedJWT.getClaim("role").asString();

        return ParsedToken.builder()
                .email(email)
                .role(role)
                .build();
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    private static class ParsedToken {
        private String email;
        private String role;
    }
}
