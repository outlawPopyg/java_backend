package org.outlaw.springsecurity;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.outlaw.springsecurity.models.User;
import org.outlaw.springsecurity.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class SpringSercurity01Application {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }


    public CommandLineRunner commandLineRunner(UserRepository repository, PasswordEncoder encoder) {
        return args -> {
            User user = User.builder()
                    .email("kalim.ahmetchin2003@gmail.com")
                    .hashPassword(encoder.encode("outlaw"))
                    .role(User.Role.USER)
                    .state(User.State.CONFIRMED)
                    .build();

            repository.save(user);
        };
    }

    public static void main(String[] args) {
        SpringApplication.run(SpringSercurity01Application.class, args);
    }

}
