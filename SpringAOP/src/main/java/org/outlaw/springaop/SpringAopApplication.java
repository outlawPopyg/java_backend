package org.outlaw.springaop;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@Slf4j
public class SpringAopApplication {

//    @Bean
    public CommandLineRunner lineRunner() {
        return args -> {
            someMethod();
        };
    }

//    @LogExecutionTime
//    @Bean
    public void someMethod() {
        try {
            Thread.sleep(1200);
            log.info("someMethod()");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {
        SpringApplication.run(SpringAopApplication.class, args);
    }

}
