package org.outlaw.springaop;

import com.example.LogRequestInfo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class BaseController {

    @GetMapping("/hello")
    @LogRequestInfo
    public String someController() {
        return "hello world";
    }
}



