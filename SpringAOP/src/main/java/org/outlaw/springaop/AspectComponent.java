package org.outlaw.springaop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
public class AspectComponent {
    @Around("@annotation(LogExecutionTime)")
    public Object logExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {
        long start = System.currentTimeMillis();

        Object proceed = joinPoint.proceed();

        long executionTime = System.currentTimeMillis() - start;

        log.info(joinPoint.getSignature() + " executed in " + executionTime + " ms");
        return proceed;
    }

    @AfterReturning(pointcut = "execution(* org.outlaw.springaop..*.*(..)) && !somePointCut()", returning = "retVal")
    public String f(Object retVal) {
        return retVal.toString();
    }

    @Pointcut("execution(void org.outlaw.springaop..*.*(..))")
    public void somePointCut() {}
}
