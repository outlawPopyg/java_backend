package org.outlaw.springcache.service;

import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.outlaw.springcache.models.User;
import org.outlaw.springcache.repository.UserRepository;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl {
    private final UserRepository userRepository;

    @Cacheable(value = "users", key = "#name")
    public User create(String name, String email) {
        System.out.println("Creating user with parameters " + name + " " + email);
        return userRepository.save(new User(name, email));
    }

    @Cacheable("users")
    public User get(Long id) {
        System.out.println("getting user by id: " + id);
        return userRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("User not found"));
    }

    @Cacheable(value = "users", key = "#user.name")
    public User createOrReturnCached(User user) {
        System.out.println("creating user: " + user);
        return userRepository.save(user);
    }

    @CachePut(value = "users", key = "#user.name")
    public User createAndRefreshCache(User user) {
        System.out.println("creating user: " + user);
        return userRepository.save(user);
    }


    public void delete(Long id) {
        System.out.println("deleting user by id: " + id);
        userRepository.deleteById(id);
    }

    @CacheEvict("users")
    public void deleteAndEvict(Long id) {
        System.out.println("deleting user by id: " + id);
        userRepository.deleteById(id);
    }


    public List<User> getAll() {
        return userRepository.findAll();
    }
}
