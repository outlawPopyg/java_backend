package org.outlaw.springcache.repository;

import org.outlaw.springcache.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
}
