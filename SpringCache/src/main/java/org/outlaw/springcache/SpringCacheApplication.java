package org.outlaw.springcache;

import com.google.common.cache.CacheBuilder;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.outlaw.springcache.models.User;
import org.outlaw.springcache.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.util.concurrent.TimeUnit;

@SpringBootApplication
@EnableCaching
@Slf4j
@RequiredArgsConstructor
public class SpringCacheApplication {

    private final UserServiceImpl userService;
    private final ApplicationContext context;

    public void getAndPrint(Long id) {
        log.info("user found: {}", userService.get(id));
    }

    public void createAndPrint(String name, String email) {
        log.info("created user: {}", userService.create(name, email));
    }

//    @Bean
//    public CommandLineRunner runner1() {
//        return (args) -> {
//            User user1 = userService.create(new User("Kalim", "kalim@mail.ru"));
//            User user2 = userService.create(new User("John", "john@mail.ru"));
//
//            getAndPrint(user1.getId());
//            getAndPrint(user2.getId());
//            getAndPrint(user1.getId());
//            getAndPrint(user2.getId());
//        };
//    }

//    @Bean
    public CommandLineRunner runner2() {
        return args -> {
            createAndPrint("Kalim", "kalim@mail.ru");
            createAndPrint("Kalim", "kalim@gmail.com");
            createAndPrint("Andrew", "andrew@mail.ru");

            log.info("all entries are below:");

            userService.getAll().forEach(user -> log.info(user.toString()));

            System.out.println(context);
            int i = 0;


//            Creating user with parameters Kalim kalim@mail.ru
//            created user: User(id=1, name=Kalim, email=kalim@mail.ru)
//            created user: User(id=1, name=Kalim, email=kalim@mail.ru)
//            Creating user with parameters Andrew andrew@mail.ru
//            created user: User(id=2, name=Andrew, email=andrew@mail.ru)
//            all entries are below:
//            User(id=1, name=Kalim, email=kalim@mail.ru)
//            User(id=2, name=Andrew, email=andrew@mail.ru)
        };
    }


//    @Bean
    public CommandLineRunner runner3() {
        return args -> {
            User user1 = userService.createOrReturnCached(new User("Kalim", "kalim@mail.ru"));
            System.out.println("created user 1: " + user1);

            User user2 = userService.createOrReturnCached(new User("Kalim", "another@mail.ru"));
            System.out.println("created user 2: " + user2);

            User user3 = userService.createAndRefreshCache(new User("Kalim", "outlaw@mail.ru"));
            System.out.println("created user 3: " + user3);

            User user4 = userService.createOrReturnCached(new User("Kalim", "andrew@mail.ru"));
            System.out.println("created user 4: " + user4);

//            creating user: User(id=null, name=Kalim, email=kalim@mail.ru)
//            created user 1: User(id=1, name=Kalim, email=kalim@mail.ru)
//            created user 2: User(id=1, name=Kalim, email=kalim@mail.ru)
//            creating user: User(id=null, name=Kalim, email=outlaw@mail.ru)
//            created user 3: User(id=2, name=Kalim, email=outlaw@mail.ru)
//            created user 4: User(id=2, name=Kalim, email=outlaw@mail.ru)
        };
    }

//    @Bean
    public CommandLineRunner runner4() {
        return args -> {
            User user1 = userService.create("Kalim", "kalim@mail.ru");
            System.out.println(userService.get(user1.getId()));

            User user2 = userService.create("Anton", "anton@mail.com");
            System.out.println(userService.get(user2.getId()));

            userService.delete(user1.getId());
            userService.deleteAndEvict(user2.getId());

            System.out.println(userService.get(user1.getId()));
            System.out.println(userService.get(user2.getId()));

//            Creating user with parameters Kalim kalim@mail.ru
//            getting user by id: 1
//            User(id=1, name=Kalim, email=kalim@mail.ru)
//            Creating user with parameters Anton anton@mail.com
//            getting user by id: 2
//            User(id=2, name=Anton, email=anton@mail.com)
//            deleting user by id: 1
//            deleting user by id: 2
//            User(id=1, name=Kalim, email=kalim@mail.ru)
//            EntityNotFoundException
        };
    }

    @Bean("habrCacheManager")
    public CacheManager cacheManager() {
        return new ConcurrentMapCacheManager() {
            @Override
            protected Cache createConcurrentMapCache(String name) {
                return new ConcurrentMapCache(
                        name,
                        CacheBuilder.newBuilder()
                                .expireAfterWrite(1, TimeUnit.SECONDS)
                                .build().asMap(),
                        false
                );
            }
        };
    }


    @Bean
    public CommandLineRunner runner5() {
        return args -> {
            User user1 = userService.createOrReturnCached(new User("Kalim", "kalim@mail.ru"));
            System.out.println(userService.get(user1.getId()));

            User user2 = userService.createOrReturnCached(new User("Kalim", "kalim@mail.ru"));
            System.out.println(userService.get(user2.getId()));

            Thread.sleep(1500);
            User user3 = userService.createOrReturnCached(new User("Kalim", "kalim@mail.ru"));
            System.out.println(userService.get(user3.getId()));
        };
    }

    public static void main(String[] args) {
        SpringApplication.run(SpringCacheApplication.class, args);
    }

}
