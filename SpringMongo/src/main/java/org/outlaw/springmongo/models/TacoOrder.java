package org.outlaw.springmongo.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.time.Instant;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document
public class TacoOrder implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    private String id;
    private Instant placedAt;
    private List<Taco> tacos;

    public void addTaco(Taco taco) {
        tacos.add(taco);
    }
}
