package org.outlaw.springmongo.repositories;

import org.outlaw.springmongo.models.TacoOrder;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends CrudRepository<TacoOrder, String> {
}
