package org.outlaw.springmongo;

import org.outlaw.springmongo.models.Ingredient;
import org.outlaw.springmongo.models.Taco;
import org.outlaw.springmongo.models.TacoOrder;
import org.outlaw.springmongo.repositories.IngredientRepository;
import org.outlaw.springmongo.repositories.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.time.Instant;
import java.util.List;

@SpringBootApplication
public class SpringMongoApplication {
    @Autowired
    private IngredientRepository ingredientRepository;
    @Autowired
    private OrderRepository orderRepository;

    @Bean
    public CommandLineRunner commandLineRunner() {
        return args -> {
            TacoOrder tacoOrder = orderRepository.findById("6413680f28125c1ceea96bed").get();
            tacoOrder.addTaco(Taco.builder().name("Empty Taco").build());

            orderRepository.save(tacoOrder);
        };
    }
    public static void main(String[] args) {
        SpringApplication.run(SpringMongoApplication.class, args);
    }

}
