package org.outlaw.models;

import lombok.Data;

@Data
public class Event {
    private int id;
    private String competitionName;
    private String team1;
    private String team2;
}