package org.outlaw;

import org.outlaw.models.FonbetLive;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class SpringRestApplication {

    private static final String URL =
            "https://line52w.bk6bba-resources.com/line/desktop/topEvents3?place=live&sysId=1&lang=ru&salt=2cddb22s9vdlfdo1zdq&supertop=4&scopeMarket=1600";


    @Bean
    public RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());

        return restTemplate;
    }

    @Bean
    public CommandLineRunner commandLineRunner(RestTemplate restTemplate) {


        return args -> {
            System.out.println(restTemplate.getForObject(URL, FonbetLive.class));
        };
    }
    public static void main(String[] args) {
        SpringApplication.run(SpringRestApplication.class, args);
    }

}
