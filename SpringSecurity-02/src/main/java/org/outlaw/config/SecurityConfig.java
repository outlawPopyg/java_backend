package org.outlaw.config;

import org.outlaw.model.User;
import org.outlaw.repository.UserRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Configuration
@EnableGlobalMethodSecurity(
        prePostEnabled = true,
        securedEnabled = true,
        jsr250Enabled = true)
public class SecurityConfig extends GlobalMethodSecurityConfiguration {
    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    // @Bean -> реализация UserDetailsService в памяти (InMemory), в ней нельзя добавлять новых пользаков в runtim
//    public UserDetailsService inMemoryUserDetailsService(PasswordEncoder encoder) {
//        List<UserDetails> userList = new ArrayList<>();
//
//        userList.add(
//                new User("buzz", encoder.encode("password"),
//                        List.of(new SimpleGrantedAuthority("ROLE_USER")))
//        );
//
//        userList.add(
//                new User("woody", encoder.encode("password"),
//                        List.of(new SimpleGrantedAuthority("ROLE_USER")))
//        );
//
//        return new InMemoryUserDetailsManager(userList);
//    }

    @Bean
    public UserDetailsService userDetailsService(UserRepository repository) {
        return username -> {
            User user = repository.findByUsername(username);
            if (user != null) {
                return user;
            }

            throw new UsernameNotFoundException(String.format("User '%s' not found", username));
        };
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        return http
                .authorizeRequests()
                .antMatchers("/design", "/orders")
                .access("hasRole('USER')")
                .antMatchers("/", "/**").permitAll()
                .and()
                    .formLogin()
                    .loginPage("/login")
                    .defaultSuccessUrl("/") // -> перенаправить на эту страницу в случае успешной аутентификации
    //                .loginProcessingUrl("/authenticate") // -> переопределяем базовый url на вход
    //                .usernameParameter("user") // -> переопределяем дефолтное значение с именем пользователя
    //                .passwordParameter("pwd") // -> переопределяем дефолтное значение с паролем
                .and()
                    .logout()
                    .logoutSuccessUrl("/login")
                .and()
                .build();
    }
}
