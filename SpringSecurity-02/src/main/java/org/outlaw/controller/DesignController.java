package org.outlaw.controller;

import org.outlaw.model.User;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/design")
public class DesignController {
    @GetMapping
    public String design(@AuthenticationPrincipal User user /* получить аутентифицированного пользака */) {
        return user.getUsername();
    }
}
